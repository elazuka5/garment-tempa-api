'use strict'

const ModelFilter = use('ModelFilter')

class SkillFilter extends ModelFilter {
  name (name) {
    return this.where('skillName', 'LIKE', `%${name}%`)
  }
}

module.exports = SkillFilter
