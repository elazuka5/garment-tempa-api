'use strict'

const ModelFilter = use('ModelFilter')

class ProjectFilter extends ModelFilter {
  name (name) {
    return this.related('userContact', 'name', 'LIKE', `%${name}%`)
  }
}

module.exports = ProjectFilter
