'use strict'

const ModelFilter = use('ModelFilter')

class PaymentFilter extends ModelFilter {
  name (name) {
    return this.related('planProject', 'name', 'LIKE', `%${name}%`)
  }
}

module.exports = PaymentFilter
