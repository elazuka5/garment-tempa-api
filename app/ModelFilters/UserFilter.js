'use strict'

const ModelFilter = use('ModelFilter')

class UserFilter extends ModelFilter {
  fullName (name) {
    return this.related('userContact', 'fullName', 'LIKE', `%${name}%`)
  }

  skill (skill) {
    return this.related('skill', 'skill', 'LIKE', `%${skill}%`)
  }

  userName (userName) {
    return this.where('userName', 'LIKE', `%${userName}%`)
  }

  phoneNumber (phoneNumber) {
    return this.related('userContact', 'phoneNumber', 'LIKE', `%${phoneNumber}%`)
  }

  nip (nip) {
    return this.related('userContact', 'nip', 'LIKE', `%${nip}%`)
  }

  role (role) {
    return this.related('role', 'name', 'LIKE', `%${role}%`)
  }

  // domicile (regencie) {
  //   return this.related('user_contacts', function () { 
  //     this.whereHas('district', (builder) => {
  //       builder.where(function () {
  //         this.orWhere('name', 'LIKE', `%${regencie}%`)

  //         this.orWhereHas('regencies', (builder) => {
  //           builder.orWhere('name', 'LIKE', `%${regencie}%`)
  //         })
  //       })
  //     })
  //   })
  // }

}

module.exports = UserFilter
