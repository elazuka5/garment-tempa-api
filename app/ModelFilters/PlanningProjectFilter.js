'use strict'

const ModelFilter = use('ModelFilter')

class PlanningProjectFilter extends ModelFilter {
  name (name) {
    return this.where('name', 'LIKE', `%${name}%`)
  }
}

module.exports = PlanningProjectFilter
