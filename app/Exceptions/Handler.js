'use strict'

const BaseExceptionHandler = use('BaseExceptionHandler')

class ExceptionHandler extends BaseExceptionHandler {

  async handle (error, { request, response }) {
    switch (error.name) {
      case 'ExpiredJwtToken':
        response.header('code', 401)
          .safeHeader('shortCode', 'SESSION_EXPIRED')
          .status(error.status)
          .send({
            status: {
              code: error.status,
              message: 'Session Expired',
              shortCode: 'SESSION_EXPIRED'
            }
          })
        break

      case 'InvalidJwtToken':
        response.header('code', 401)
          .safeHeader('shortCode', 'INVALID_TOKEN')
          .status(error.status)
          .send({
            status: {
              code: error.status,
              message: 'Login Required',
              shortCode: 'INVALID_TOKEN'
            }
          })
        break

      case 'InvalidRefreshToken':
        response.header('code', 401)
          .safeHeader('shortCode', 'INVALID_TOKEN')
          .status(error.status)
          .send({
            status: {
              code: error.status,
              message: 'Login Required',
              shortCode: 'INVALID_TOKEN'
            }
          })
        break
    
      default:
        response.status(error.status).send({
          code: error.status,
          message: error.message
        })
        break
    }
  }

  /**
   * Report exception for logging or debugging.
   *
   * @method report
   *
   * @param  {Object} error
   * @param  {Object} options.request
   *
   * @return {void}
   */
  async report (error, { request }) {
  }
}

module.exports = ExceptionHandler
