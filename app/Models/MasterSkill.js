'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const SkillFilter = use('App/ModelFilters/SkillFilter')

class MasterSkill extends Model {
  static boot () {
    super.boot()
    this.addTrait('@provider:Filterable', SkillFilter)
  }
}

module.exports = MasterSkill
