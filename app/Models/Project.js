'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuidv4 } = use('uuid');
const ProjectFilter = use('App/ModelFilters/ProjectFilter')

class Project extends Model {
  static boot () {
    super.boot()
    this.addHook('afterFind', async (payload) => {
      if(Array.isArray(payload)){
        for (let item of payload) {
          if(item.description){
            item.description = JSON.parse(item.description)
          }
          if(item.skill){
            item.skill = JSON.parse(item.skill)
          }
          if(item.size){
            item.size = JSON.parse(item.size)
          }
        }
      }

      if(payload.skill){
        payload.skill = JSON.parse(payload.skill)
      }
      if(payload.size){
        payload.size = JSON.parse(payload.size)
      }
      if(payload.description){
        payload.description = JSON.parse(payload.description)
      }
    })

    this.addHook('beforeUpdate', async (payload) => {
      if(Array.isArray(payload)){
        for (let item of payload) {
          if(item.description){
            item.description = JSON.stringify(item.description)
          }
          if(item.skill){
            item.skill = JSON.stringify(item.skill)
          }
          if(item.size){
            item.size = JSON.stringify(item.size)
          }
        }
      }

      if(payload.skill){
        payload.skill = JSON.stringify(payload.skill)
      }
      if(payload.size){
        payload.size = JSON.stringify(payload.size)
      }
      if(payload.description){
        payload.description = JSON.stringify(payload.description)
      }
    })

    this.addHook('afterFetch', async (payload) => {
      if(Array.isArray(payload)){
        for (let item of payload) {
          if(item.description){
            item.description = JSON.parse(item.description)
          }
          if(item.skill){
            item.skill = JSON.parse(item.skill)
          }
          if(item.size){
            item.size = JSON.parse(item.size)
          }
        }
      }

      if(payload.skill){
        payload.skill = JSON.parse(payload.skill)
      }
      if(payload.size){
        payload.size = JSON.parse(payload.size)
      }
      if(payload.description){
        payload.description = JSON.parse(payload.description)
      }
    })

    this.addHook('afterPaginate', async (payload) => {
      if(Array.isArray(payload)){
        for (let item of payload) {
          if(item.description){
            item.description = JSON.parse(item.description)
          }
          if(item.skill){
            item.skill = JSON.parse(item.skill)
          }
          if(item.size){
            item.size = JSON.parse(item.size)
          }
        }
      }

      if(payload.skill){
        payload.skill = JSON.parse(payload.skill)
      }
      if(payload.size){
        payload.size = JSON.parse(payload.size)
      }
      if(payload.description){
        payload.description = JSON.parse(payload.description)
      }
    })
    this.addHook('beforeCreate', async (payload) => {
      payload.uid = `TMP::PROJECT::${uuidv4()}`
    })
    this.addTrait('@provider:Filterable', ProjectFilter)
  }

  static get primaryKey() {
    return 'uid'
  }

  static get incrementing () {
    return false
  }

  planProject() {
    return this.belongsTo('App/Models/PlanningProject', 'projectId', 'uid')
  }

  sizeClothes() {
    return this.hasOne('App/Models/SizeClothe', 'uid', 'projectId')
  }

  sizePants() {
    return this.hasOne('App/Models/SizePant', 'uid', 'projectId')
  }

  process() {
    return this.hasMany('App/Models/ProjectProcess', 'uid', 'projectId')
  }
}

module.exports = Project
