'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuidv4 } = use('uuid');

class SizePant extends Model {
  static boot () {
    super.boot()
    this.addHook('beforeCreate', async (payload) => {
      payload.uid = `TMP::PANTS::${uuidv4()}`
    })
  }

  static get primaryKey() {
    return 'uid'
  }

  static get incrementing () {
    return false
  }

  project() {
    return this.belongsTo('App/Models/Project', 'projectId', 'uid')
  }
}

module.exports = SizePant
