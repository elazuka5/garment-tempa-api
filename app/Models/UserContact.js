'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuidv4 } = use('uuid');

class UserContact extends Model {
  static boot () {
    super.boot()
    this.addHook('beforeCreate', async (payload) => {
      payload.uid = `TMP::CONTACT::${uuidv4()}`
    })
  }

  static get primaryKey() {
    return 'uid'
  }

  static get incrementing () {
    return false
  }

  users() {
    return this.belongsTo('App/Models/User', 'userId', 'uid')
  }
}

module.exports = UserContact
