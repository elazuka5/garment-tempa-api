'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class UserType extends Model {
  static get hidden() {
    return ['id', 'isDeleted', 'created_at', 'updated_at', 'code'];
  }
}

module.exports = UserType
