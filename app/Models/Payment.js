'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuidv4 } = use('uuid');
const PaymentFilter = use('App/ModelFilters/PaymentFilter')

class Payment extends Model {
  static boot () {
    super.boot()

    this.addHook('afterFind', async (payload) => {
      if(Array.isArray(payload)){
        for (let item of payload) {
          if(item.invoicePath){
            item.invoicePath = `http://cdn.tempa-bersama.co.id/${item.invoicePath}`
          }
          if(item.pathExcel){
            item.pathExcel = `http://cdn.tempa-bersama.co.id/${item.pathExcel}`
          }
        }
      }
      if(payload.invoicePath){
        payload.invoicePath = `http://cdn.tempa-bersama.co.id/${item.invoicePath}`
      }
      if(payload.pathExcel){
        payload.pathExcel = `http://cdn.tempa-bersama.co.id/${item.pathExcel}`
      }
    })

    this.addHook('afterFetch', async (payload) => {
      if(Array.isArray(payload)){
        for (let item of payload) {
          if(item.invoicePath){
            item.invoicePath = `http://cdn.tempa-bersama.co.id/${item.invoicePath}`
          }
          if(item.pathExcel){
            item.pathExcel = `http://cdn.tempa-bersama.co.id/${item.pathExcel}`
          }
        }
      }
      if(payload.invoicePath){
        payload.invoicePath = `http://cdn.tempa-bersama.co.id/${item.invoicePath}`
      }
      if(payload.pathExcel){
        payload.pathExcel = `http://cdn.tempa-bersama.co.id/${item.pathExcel}`
      }
    })

    this.addHook('afterPaginate', async (payload) => {
      if(Array.isArray(payload)){
        for (let item of payload) {
          if(item.invoicePath){
            item.invoicePath = `http://cdn.tempa-bersama.co.id/${item.invoicePath}`
          }
          if(item.pathExcel){
            item.pathExcel = `http://cdn.tempa-bersama.co.id/${item.pathExcel}`
          }
        }
      }
      if(payload.invoicePath){
        payload.invoicePath = `http://cdn.tempa-bersama.co.id/${item.invoicePath}`
      }
      if(payload.pathExcel){
        payload.pathExcel = `http://cdn.tempa-bersama.co.id/${item.pathExcel}`
      }
    })

    this.addHook('beforeCreate', async (payload) => {
      payload.uid = `TMP::PAY::${uuidv4()}`
    })
    this.addTrait('@provider:Filterable', PaymentFilter)
  }

  static get primaryKey() {
    return 'uid'
  }

  static get incrementing () {
    return false
  }

  planProject() {
    return this.belongsTo('App/Models/PlanningProject', 'planProjectId', 'uid')
  }

  project() {
    return this.belongsTo('App/Models/Project', 'projectId', 'uid')
  }

  user() {
    return this.belongsTo('App/Models/User', 'userId', 'uid')
  }

  skill() {
    return this.belongsTo('App/Models/MasterSkill', 'skill', 'skillName')
  }

  // invoice() {
  //   return this.belongsTo('App/Models/Invoice', 'invoiceId', 'id')
  // }
}

module.exports = Payment
