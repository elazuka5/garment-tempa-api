'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuidv4 } = use('uuid');

class ProjectProcess extends Model {
  static boot () {
    super.boot()
    // this.addHook('afterFind', async (payload) => {
    //   if(Array.isArray(payload)){
    //     for (let item of payload) {
    //       if(item.qc){
    //         item.qc = JSON.parse(item.qc)
    //       }
    //       if(item.user){
    //         item.user = JSON.parse(item.user)
    //       }
    //     }
    //   }

    //   if(payload.user){
    //     payload.user = JSON.parse(payload.user)
    //   }
    //   if(payload.qc){
    //     payload.qc = JSON.parse(payload.qc)
    //   }
    // })

    // this.addHook('beforeUpdate', async (payload) => {
    //   if(Array.isArray(payload)){
    //     for (let item of payload) {
    //       if(item.qc){
    //         item.qc = JSON.stringify(item.qc)
    //       }
    //       if(item.skill){
    //         item.skill = JSON.stringify(item.skill)
    //       }
    //       if(item.user){
    //         item.user = JSON.stringify(item.user)
    //       }
    //     }
    //   }

    //   if(payload.skill){
    //     payload.skill = JSON.stringify(payload.skill)
    //   }
    //   if(payload.user){
    //     payload.user = JSON.stringify(payload.user)
    //   }
    //   if(payload.qc){
    //     payload.qc = JSON.stringify(payload.qc)
    //   }
    // })

    // this.addHook('afterFetch', async (payload) => {
    //   if(Array.isArray(payload)){
    //     for (let item of payload) {
    //       if(item.qc){
    //         item.qc = JSON.parse(item.qc)
    //       }
    //       if(item.user){
    //         item.user = JSON.parse(item.user)
    //       }
    //     }
    //   }

    //   if(payload.user){
    //     payload.user = JSON.parse(payload.user)
    //   }
    //   if(payload.qc){
    //     payload.qc = JSON.parse(payload.qc)
    //   }
    // })

    // this.addHook('afterPaginate', async (payload) => {
    //   if(Array.isArray(payload)){
    //     for (let item of payload) {
    //       if(item.qc){
    //         item.qc = JSON.parse(item.qc)
    //       }
    //       if(item.user){
    //         item.user = JSON.parse(item.user)
    //       }
    //     }
    //   }

    //   if(payload.user){
    //     payload.user = JSON.parse(payload.user)
    //   }
    //   if(payload.qc){
    //     payload.qc = JSON.parse(payload.qc)
    //   }
    // })

    this.addHook('beforeCreate', async (payload) => {
      payload.uid = `TMP::PROCESS::${uuidv4()}`
    })
  }

  static get primaryKey() {
    return 'uid'
  }

  static get incrementing () {
    return false
  }

  qc() {
    return this.belongsTo('App/Models/User', 'qcId', 'uid')
  }

  user() {
    return this.belongsTo('App/Models/User', 'userId', 'uid')
  }

  project() {
    return this.belongsTo('App/Models/Project', 'projectId', 'uid')
  }

}

module.exports = ProjectProcess
