'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const config = use('Config')

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')
const { v4: uuidv4 } = use('uuid');
const UserFilter = use('App/ModelFilters/UserFilter')

class User extends Model {
  static boot () {
    super.boot()
    this.addHook('beforeCreate', async (payload) => {
      switch (payload.userType) {
        case 1:
          payload.uid = `TMP::SUPER_ADM::${uuidv4()}`
          break;
        case 2:
          payload.uid = `TMP::ADM::${uuidv4()}`
          break;
        case 3:
          payload.uid = `TMP::TUKANG::${uuidv4()}`
          break;
        case 4:
          payload.uid = `TMP::SH::${uuidv4()}`
          break;
        case 5:
          payload.uid = `TMP::QC::${uuidv4()}`
          break;
        case 6:
          payload.uid = `TMP::FIT::${uuidv4()}`
          break;
        default:
          break;
      }
    })
    this.addTrait('@provider:Filterable', UserFilter)
  }

  // static get connection() {
  //   return this.useConnection !== "undefined" ? this.useConnection : config.get('database.connection')
  // }

  // static setUseConnection(access) {
  //   return access
  // }

  static get hidden() {
    return ['id', 'isDeleted'];
  }

  static get primaryKey() {
    return 'uid'
  }

  static get incrementing () {
    return false
  }

  tokens() {
    return this.hasMany('App/Models/Token', 'uid', 'userId')
  }

  userContact() {
    return this.hasOne('App/Models/UserContact', 'uid', 'userId')
  }

  role() {
    return this.hasOne('App/Models/UserType', 'userType', 'id')
  }

  skill() {
    return this.hasOne('App/Models/Skill', 'uid', 'userId')
  }

  payment() {
    return this.hasMany('App/Models/Payment', 'uid', 'userId')
  }

  process() {
    return this.hasMany('App/Models/ProjectProcess', 'uid', 'userId')
  }

}

module.exports = User
