'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuidv4 } = use('uuid');
const PlanningProjectFilter = use('App/ModelFilters/PlanningProjectFilter')

class PlanningProject extends Model {
  static boot () {
    super.boot()
    this.addHook('afterFind', async (payload) => {
      if(Array.isArray(payload)){
        for (let item of payload) {
          if(item.skill){
            item.skill = JSON.parse(item.skill)
          }
          if(item.size){
            item.size = JSON.parse(item.size)
          }
        }
      }

      if(payload.skill){
        payload.skill = JSON.parse(payload.skill)
      }
      if(payload.size){
        payload.size = JSON.parse(payload.size)
      }
    })

    this.addHook('beforeUpdate', async (payload) => {
      if(Array.isArray(payload)){
        for (let item of payload) {
          if(item.skill){
            item.skill = JSON.stringify(item.skill)
          }
          if(item.size){
            item.size = JSON.stringify(item.size)
          }
        }
      }

      if(payload.skill){
        payload.skill = JSON.stringify(payload.skill)
      }
      if(payload.size){
        payload.size = JSON.stringify(payload.size)
      }
    })

    this.addHook('afterFetch', async (payload) => {
      if(Array.isArray(payload)){
        for (let item of payload) {
          if(item.skill){
            item.skill = JSON.parse(item.skill)
          }
          if(item.size){
            item.size = JSON.parse(item.size)
          }
        }
      }

      if(payload.skill){
        payload.skill = JSON.parse(payload.skill)
      }
      if(payload.size){
        payload.size = JSON.parse(payload.size)
      }
    })

    this.addHook('afterPaginate', async (payload) => {
      if(Array.isArray(payload)){
        for (let item of payload) {
          if(item.skill){
            item.skill = JSON.parse(item.skill)
          }
          if(item.size){
            item.size = JSON.parse(item.size)
          }
        }
      }

      if(payload.skill){
        payload.skill = JSON.parse(payload.skill)
      }
      if(payload.size){
        payload.size = JSON.parse(payload.size)
      }
    })

    this.addHook('beforeCreate', async (payload) => {
      payload.uid = `TMP::PLAN_PROJECT::${uuidv4()}`
    })
    this.addTrait('@provider:Filterable', PlanningProjectFilter)
  }

  static get primaryKey() {
    return 'uid'
  }

  static get incrementing () {
    return false
  }

  project() {
    return this.hasMany('App/Models/Project', 'uid', 'projectId')
  }

  comment() {
    return this.hasMany('App/Models/ProjectComment', 'uid', 'planProjectId')
  }
}

module.exports = PlanningProject
