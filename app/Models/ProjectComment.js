'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ProjectComment extends Model {
  static get primaryKey() {
    return 'id'
  }

  static get incrementing () {
    return false
  }

  planProject() {
    return this.belongsTo('App/Models/PlanningProject', 'planProjectId', 'uid')
  }

  project() {
    return this.belongsTo('App/Models/Project', 'projectId', 'uid')
  }

  user() {
    return this.belongsTo('App/Models/User', 'userId', 'uid')
  }
}

module.exports = ProjectComment
