'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const { v4: uuidv4 } = use('uuid');

class Skill extends Model {
  static boot () {
    super.boot()
    this.addHook('afterFind', async (payload) => {
      if(Array.isArray(payload)){
        for (let item of payload) {
          if(item.skill){
            item.skill = JSON.parse(item.skill)
          }
        }
      }
      if(payload.skill){
        payload.skill = JSON.parse(payload.skill)
      }
    })

    this.addHook('afterFetch', async (payload) => {
      if(Array.isArray(payload)){
        for (let item of payload) {
          if(item.skill){
            item.skill = JSON.parse(item.skill)
          }
        }
      }
      if(payload.skill){
        payload.skill = JSON.parse(payload.skill)
      }
    })

    this.addHook('beforeUpdate', async (payload) => {
      if(Array.isArray(payload)){
        for (let item of payload) {
          if(item.skill){
            item.skill = JSON.stringify(item.skill)
          }
        }
      }
      if(payload.skill){
        payload.skill = JSON.stringify(payload.skill)
      }
    })

    this.addHook('afterPaginate', async (payload) => {
      if(Array.isArray(payload)){
        for (let item of payload) {
          if(item.skill){
            item.skill = JSON.parse(item.skill)
          }
        }
      }
      if(payload.skill){
        payload.skill = JSON.parse(payload.skill)
      }
    })

    this.addHook('beforeCreate', async (payload) => {
      payload.uid = `TMP::SKILL::${uuidv4()}`
    })
  }

  static get primaryKey() {
    return 'uid'
  }

  static get incrementing () {
    return false
  }

  users() {
    return this.belongsTo('App/Models/User', 'userId', 'uid')
  }
}

module.exports = Skill
