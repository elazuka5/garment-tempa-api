'use strict'

// SERVICES
const ResponseService = use('App/Services/ResponseService');
// const CheckToken = use('App//Middleware/CheckToken');
// const DataHeaderService = use('App/Services/DataHeaderService');
// const CheckDbService = use('App/Services/CheckDbService');
// const ApiService = use('App/Services/ApiService');
// const Base64Service = use('App/Services/Base64Service');
// SERVICES

const User = use("App/Models/User");
const Token = use("App/Models/Token");

var jwt = use('jsonwebtoken');

class AuthController {
  constructor() {
    this.res = new ResponseService
    // this.dataHeader = new DataHeaderService
    // this.base64 = new Base64Service
    // this.userControl = new UserController
    // this.fetch = new ApiService
  }

  async login({ request, response, auth }) {
    try {
      let { userName, password } = request.all()
      // User.useConnection = "anotherConnection"
      let user = await User.query()
      .with('role')
      .with('userContact')
      .where('userName', userName)
      .where('password', password)
      .first()
      if(user){
        // if(user.userType !== 2){
        await Token.query().where('userId', user.uid).delete()
        // }
        let token = await auth.withRefreshToken().generate(user, true)
        await user.tokens().create({
          token: token.token,
          type: 'jwt'
        })

        // response.plainCookie('__mtltkn', token.token, {
        //   domain: '.mentalku.com',
        //   path: '/',
        //   httpOnly: true,
        //   //maxAge: 60 * 60,
        //   expires: new Date(new Date().setHours(new Date().getHours() + 6)),
        //   secure: true,
        //   sameSite: 'lax'
        // })

        return response.status(200).json(await this.res.store('Login Berhasil', 200, 'SUCCESS', token))
      }else{
        return response.status(200).json(await this.res.store('Username atau Password Tidak Ditemukan', 401, 'FAILED'))
      }
    } catch (error) {
      console.log('login', error);
      return response.status(401).json(await this.res.store('Terjadi Kesalahan', 401, 'UNAUTHORIZED', error))
    }
  }

  async logout({ response, request, auth }) {
    try {
      const authUser = await auth.getUser()
      const token = await Token.findBy('userId', authUser.uid) 
      if(token){
        await token.delete()
        await token.save()
      }
      return response.status(200).json(await this.res.store('Logout success', 200, 'SUCCESS'))
    } catch (error) {
      console.log('logout', error);
      return response.status(400).json(await this.res.store(error, 400, 'FAILED'))
    }
  }

  async refreshToken({ request, response, auth }) {
    try {
      const data = request.all()
      const newToken = await auth.generateForRefreshToken(data.refreshToken)
      if (newToken) {
        const jwtToken = jwt.decode(newToken.token)
        await Token.query().where('userId', jwtToken.uid).delete()
        const user = await User.query()
        .with('role')
        .with('userContact')
        .where('uid', jwtToken.uid)
        .first()
        // if (user.userType !== 1) {
        //   user.strength = await this.res.strengthCheck(user.uid)        
        // }
        let token = await auth.withRefreshToken().generate(user, true)          
          // response.plainCookie('__acetkn', token.token, {
            //   domain: '.localhost',
            //   path: '/',
            //   httpOnly: true,
            //   expires: new Date(new Date().setHours(new Date().getHours() + 6)),
            //   secure: true,
            //   sameSite: 'lax'
          // })
        return response.status(200).json(await this.res.store('Check success', 200, 'SUCCESS', token))
      } else {
        return response.status(200).json(await this.res.store('Invalid refresh token', 401, 'UNAUTHORIZE'))
      }
    } catch (error) {
      console.log('refresh', error);
      return response.status(401).json(await this.res.store('Terjadi Kesalahan', 401, 'UNAUTHORIZED', error))
    }
  }

  async changePass ({ request, response, auth }) {
    try {
      const authUser = await auth.getUser()
      const data = request.all()
      const isUid = data.uid ? data.uid : authUser.uid
      const user = await User.find(isUid)
      if(user){
        user.merge({
          password: data.newPassword
        })
        await user.save()
        return response.status(200).json(await this.res.store('Berhasil Mengubah Password', 200, 'SUCCESS'))
      }else{
        return response.status(200).json(await this.res.store('User Tidak Ditemukan', 400, 'FAILED'))
      }
    } catch (error) {
      console.log('change pass', error);
      return response.status(401).json(await this.res.store('Terjadi Kesalahan', 401, 'UNAUTHORIZED', error))
    }
  }

}

module.exports = AuthController
