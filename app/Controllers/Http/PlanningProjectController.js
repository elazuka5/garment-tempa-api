'use strict'

// SERVICES
const ResponseService = use('App/Services/ResponseService');
// const CheckToken = use('App//Middleware/CheckToken');
const DataHeaderService = use('App/Services/DataHeaderService');
// const CheckDbService = use('App/Services/CheckDbService');
// const ApiService = use('App/Services/ApiService');
const Base64Service = use('App/Services/Base64Service');
const RedisService = use('App/Services/RedisService');
// SERVICES

// ADONIS
const Database = use('Database')
const Event = use('Event')
const Helpers = use('Helpers')
const Drive = use('Drive')
const Env = use('Env')
const fs = use('fs')
const csv = use('csv-parser');
const Redis = use('Redis')
// ADONIS

// MODELS
const User = use('App/Models/User')
const UserContact = use('App/Models/UserContact')
const Skill = use('App/Models/Skill')
const MasterSkill = use('App/Models/MasterSkill')
const Project = use('App/Models/Project')
// const MongoProject = use('App/Models/Mongo/Project')
const PlanningProject = use('App/Models/PlanningProject')
const SizeClothe = use('App/Models/SizeClothe')
const SizePant = use('App/Models/SizePant')
// MODELS

// MODULE
var base64Img = use('base64-img');
var { degrees, PDFDocument, rgb, StandardFonts } = use('pdf-lib');
var moment = use('moment')
const QRCode = use('qrcode')
const Papa = use('papaparse');
const CsvToJson = use('csvtojson')
const { parseFile } = use('fast-csv');
const csvtojson = use('csvtojson');
// MODULE

class PlanningProjectController {
  constructor() {
    this.res = new ResponseService
    this.dataHeader = new DataHeaderService
    this.base64 = new Base64Service
    this.redis = new RedisService
    // this.checkDB = new CheckDbService
    // this.fetch = new ApiService
  }

  async indexPlanProject ({ request, response, auth }) {
    try {
      await auth.getUser()
      var headerData = await this.dataHeader.getHeaderData(request)
      var { userType, current, filter, pageSize } = headerData
      var filterParams = {
        name: filter ? (filter.type === 'name' ? filter.search : '') : '',
        // skill: filter ? (filter.type === 'skill' ? filter.search : '') : '',
        // userName: filter ? (filter.type === 'userName' ? filter.search : '') : '',
        // phoneNumber: filter ? (filter.type === 'phoneNumber' ? filter.search : '') : '',
        // nip: filter ? (filter.type === 'nip' ? filter.search : '') : '',
      }
      var user = PlanningProject.query()
      // .with('comment')
      .withCount('comment as countComment')
      // .whereNotIn('userType', [1])
      .where('isDeleted', '=', false)

      // const cachedUsers = await Redis.get('*')
      // const cachedUsers = await Redis.keys('*')
      // if (cachedUsers) {
        // return JSON.parse(cachedUsers)
      // }
      // const parse = JSON.parse(cachedUsers)
      // console.log(23, parse.name);
      // var pp = await PlanningProject.all()
      // await Redis.set('project', JSON.stringify(pp))

      // await Redis.del(cachedUsers[0])

      // let qq = await this.redis.get('project', 'project_5')
      // let getIndex = await Redis.llen('qq')
      // let qq = await Redis.lrange('qq', 0, getIndex)

      // let pp = qq.map(item => JSON.parse(item))
      // console.log(getIndex);
      // qq.forEach(item => {
      //   JSON.parse(item)
      //   console.log(item);
      // })
      
      var planQuery = await user
      .filter(filterParams)
      .orderBy(`created_at`, filter && filter.orderBy ? filter.orderBy : 'desc')
      .paginate(current || 1, pageSize || 10)
      var planJson = planQuery.toJSON()
      planJson.data.length > 0 && planJson.data.forEach(item => {
        var progress = Math.ceil(item.count / item.total * 100);
        item.progress = progress
        item.totalComment = item.__meta__.countComment
      })
      return response.status(200).json(await this.res.store('Get List Success', 200, 'SUCCESS', planJson))
    } catch (error) {
      console.log('list plan project', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }

  async storePlanProject ({ request, response, auth }) {
    try {
      await auth.check()
      let data = request.all()
      let cachedProject = await Redis.keys('*')
      if(cachedProject){
        await Redis.flushall()
      }
      await Redis.set('planProject', JSON.stringify(data))
      let index = cachedProject.indexOf('planProject')
      let key = cachedProject[index]
      return response.status(200).json(await this.res.store('Berhasil Membuat Planning Project', 200, 'SUCCESS', key))
    } catch (error) {
      console.log('create plan project', error);
      return response.status(400).json(await this.res.store('Terjadi Kesalahan', 400, 'FAILED', error))
    }
  }

  async updatePlanProject ({ request, response, auth }) {
    try {
      await auth.check()
      var data = request.all()
      var project = await PlanningProject.query().with('project').where('uid', data.uid).first()
      if(project){
        await PlanningProject.query().where('uid', project.uid).update({
          name: data.name,
          day: data.day,
          total: data.total,
          deadline: data.deadline,
          notes: data.notes,
          size: data.size,
          skill: data.skill,
        })

        var max = await Project.query().where('projectId', data.uid).getMax('indexSort')
        var planing = await Project.query().where('projectId', data.uid).first();
        const validationOptions = {
          size: '10mb',
          // extnames: ['csv']
        };
        const imageFile = request.file('file', validationOptions);
        if(planing){
          if(imageFile){
            let filePath = data.name
            let fileName = `project.${imageFile.extname}`
            
            await imageFile.move(Helpers.tmpPath(`${filePath}`), {
              name: fileName,
              overwrite: true,
            });
    
            if (!imageFile.moved()) {
              return response.status(200).json(await this.res.store('Error', 400, 'FAILED', imageFile.error()))
            }
            let pathTmp = Helpers.tmpPath(`${filePath}/${fileName}`)
            let jsonArray = await csvtojson({
              delimiter: 'auto',
              maxRowLength: 10,
              checkType:true,
              nullObject: true,
            }).fromFile(pathTmp);
            let totalProject = jsonArray.length
            let totalRange = project.total - project.totalProject
            
            if(totalProject > totalRange){
              return response.status(200).json(await this.res.store('File CSV Melebihi Total Project', 400, 'FAILED'))
            }
    
            const isExist = await Drive.exists(`${filePath}/${fileName}`);
            if(isExist){
              await Drive.delete(`${filePath}/${fileName}`)
            }
    
            for (const [i, item] of jsonArray.entries()) {
              const sizePro = project.size.map(res => {
                if(Array.isArray(res.value)){
                  res.value = res.value.reduce((a, b) => (a[b]=0, a), {})
                  return res
                }
                return res
              })
              let projectSize = JSON.stringify(sizePro)
              // let arrSkill = []
              let convertSkill = project.skill.reduce((a, b) => (a[b]=null, a), {})
              // arrSkill.push(convertSkill)
              let projectSkill = JSON.stringify(convertSkill)
              await Project.create({ 
                description: JSON.stringify(item),
                size: projectSize,
                skill: projectSkill,
                projectId: project.uid,
                indexSort: i ? max + i + 1 : max + 1
              })
              await PlanningProject.query().where('uid', project.uid).update({ totalProject: i ? project.totalProject + i + 1 : project.totalProject + 1 })
            }
          }
        }else{
          if(imageFile){
            let filePath = data.name
            let fileName = `project.${imageFile.extname}`
            
            await imageFile.move(Helpers.tmpPath(`${filePath}`), {
              name: fileName,
              overwrite: true,
            });
    
            if (!imageFile.moved()) {
              return response.status(200).json(await this.res.store('Error', 400, 'FAILED', imageFile.error()))
            }

            let pathTmp = Helpers.tmpPath(`${filePath}/${fileName}`)
            let jsonArray = await csvtojson({
              maxRowLength: 10,
              checkType:true,
              nullObject: true,
            }).fromFile(pathTmp);
            let totalProject = jsonArray.length
            let totalRange = project.total - project.totalProject
            
            if(totalProject > totalRange){
              return response.status(200).json(await this.res.store('File CSV Melebihi Total Project', 400, 'FAILED'))
            }
    
            const isExist = await Drive.exists(`${filePath}/${fileName}`);
            if(isExist){
              await Drive.delete(`${filePath}/${fileName}`)
            }

            for (const [i, item] of jsonArray.entries()) {
              const sizePro = project.size.map(res => {
                if(Array.isArray(res.value)){
                  res.value = res.value.reduce((a, b) => (a[b]=0, a), {})
                  return res
                }
                return res
              })
              let projectSize = JSON.stringify(sizePro)
              // let arrSkill = []
              let convertSkill = project.skill.reduce((a, b) => (a[b]=null, a), {})
              // arrSkill.push(convertSkill)
              let projectSkill = JSON.stringify(convertSkill)
              await Project.create({ 
                description: JSON.stringify(item),
                size: projectSize,
                skill: projectSkill,
                projectId: project.uid,
                indexSort: i
              })
            }
            await PlanningProject.query().where('uid', project.uid).update({ totalProject: totalProject + project.totalProject })
          }
        }
        return response.status(200).json(await this.res.store('Berhasil Mengubah Planning Project', 200, 'SUCCESS', project))
      }else{
        return response.status(200).json(await this.res.store('Project Tidak Ditemukan', 400, 'FAILED'))
      }
    } catch (error) {
      console.log('update plan', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }

  async destroyPlanProject ({ request, response, auth }) {
    try {
      await auth.check()
      var data = request.all();
      var project = await PlanningProject.find(data.uid)
      if(project){
        await project.project().delete()
        await project.delete()
        return response.status(200).json(await this.res.store('Berhasil Menghapus Project', 200, 'SUCCESS'))
      }else{
        return response.status(200).json(await this.res.store('Project Tidak Ditemukan', 404, 'FAILED'))
      }
    } catch (error) {
      console.log('delete', error);
      return response.status(400).json(await this.res.store('Delete Project Failed', 400, 'FAILED', error))
    }
  }

  async show ({ params, request, response, auth }) {
    try {
      await auth.getUser()
      var headerData = await this.dataHeader.getHeaderData(request)
      var project = await PlanningProject.query()
      .with('comment', (builder) => {
        builder
        .with('planProject', (builder) => builder.select('planning_projects.uid', 'planning_projects.name'))
        .with('project', (builder) => builder.select('projects.uid', 'projects.description'))
        .with('user', (builder) => {
          builder.select('users.uid').with('userContact')
        })
      })
      .with('project', (builder) => {
        builder.select('projects.projectId')
        builder.whereRaw(`JSON_SEARCH(JSON_EXTRACT(projects.skill, '$**.*'), 'all', 'TMP%') is not null`)
        // builder.select('projects.projectId', Database.raw(`IF(JSON_SEARCH(JSON_EXTRACT(projects.skill, '$**.*'), 'all', 'TMP%') is not null,1,0) as isEditProject`))
      })
      .where('uid', headerData.uid)
      .where('isDeleted', '=', false)
      // .with('project', (builder) => {
      //   builder.orderByRaw('-size ASC')
      // })
      .first()

      var proJson = project && project.toJSON()
      if(project){
        proJson.isEditProject = proJson.project.length > 0 ? true : false
        return response.status(200).json(await this.res.store('Get List Success', 200, 'SUCCESS', proJson))
      }else{
        return response.status(200).json(await this.res.store('Project Tidak Ditemukan', 401, 'FAILED'))
      }
    } catch (error) {
      console.log('detail project', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }
}

module.exports = PlanningProjectController
