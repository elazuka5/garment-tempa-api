'use strict'

// SERVICES
const ResponseService = use('App/Services/ResponseService');
const Base64Service = use('App/Services/Base64Service');
// const CheckToken = use('App//Middleware/CheckToken');
const DataHeaderService = use('App/Services/DataHeaderService');
// const CheckDbService = use('App/Services/CheckDbService');
// const ApiService = use('App/Services/ApiService');
// SERVICES

// ADONIS
const Database = use('Database')
// ADONIS


class DataMasterController {
  constructor() {
    this.res = new ResponseService
    this.base64 = new Base64Service
    this.dataHeader = new DataHeaderService
    // this.checkJwt = new CheckToken
    // this.checkDB = new CheckDbService
    // this.fetch = new ApiService
  }

  async datamaster ({ request, response }) {
    try {
      const q = await this.base64.store(request.header('data'))
      const arr = q.type
      var res = {}

      for (var i = 0; i < arr.length; i++) {
        
        var table = arr[i]
        switch (table) {
          // case 'provinces':
          //   res[table] = await Database.select('*').from([table])
          //   break;

          // case 'regencies':
          //   res[table] = q.id ? await Database.select('id', 'name').from([table]).where('province_id', q.id) : await Database.select('*').from([table])
          //   break;
            
          // case 'districts':
          //   res[table] = await Database.select('id', 'name').from([table]).where('regency_id', q.id)
          //   break;
            
          // case 'villages':
          //   res[table] = await Database.select('id', 'name').from([table]).where('district_id', q.id)
          //   break;

          // case 'outlet_contacts':
          //   res[table] = await Database.select('*').from([table]).where('city', 'LIKE', `%${q.city}%`)
          //   break;

          // case 'sim_categories':
          //   res[table] = await Database.select('A', 'B', 'C', 'D', 'A & C').from([table]).where('outletId', q.uid)
          //   break;
            
          default:
            res[table] = await Database.select('*').from([table])
            break;
        }
      }
      return response.status(200).json(await this.res.store('Data master success', 200, 'SUCCESS', res))
            
    } catch (error) {
      console.log('master', error);
      return response.status(400).json(await this.res.store(error, 400, 'FAILED'))
      
    }
  }
}

module.exports = DataMasterController
