'use strict'

const csvtojson = require('csvtojson');

// SERVICES
const ResponseService = use('App/Services/ResponseService');
// const CheckToken = use('App//Middleware/CheckToken');
const DataHeaderService = use('App/Services/DataHeaderService');
// const CheckDbService = use('App/Services/CheckDbService');
// const ApiService = use('App/Services/ApiService');
const Base64Service = use('App/Services/Base64Service');
const RedisService = use('App/Services/RedisService');
const PdfService = use('App/Services/PdfService');
const ExportService = use('App/Services/ExportService');
// SERVICES

// ADONIS
const Database = use('Database')
const Event = use('Event')
const Helpers = use('Helpers')
const Drive = use('Drive')
const Env = use('Env')
const fs = use('fs')
const csv = use('csv-parser');
const Redis = use('Redis')
// const userOtp = Env.get('USERKEY_OTP')
// const passOtp = Env.get('PASSKEY_OTP')
// ADONIS

// MODELS
const User = use('App/Models/User')
const UserContact = use('App/Models/UserContact')
const Skill = use('App/Models/Skill')
const MasterSkill = use('App/Models/MasterSkill')
const Project = use('App/Models/Project')
// const MongoProject = use('App/Models/Mongo/Project')
const PlanningProject = use('App/Models/PlanningProject')
const Payment = use('App/Models/Payment')
const SizeClothe = use('App/Models/SizeClothe')
const SizePant = use('App/Models/SizePant')
const ProjectComment = use('App/Models/ProjectComment');
const ProjectProcess = use('App/Models/ProjectProcess');
// MODELS

// MODULE
var base64Img = use('base64-img');
var { degrees, PDFDocument, rgb, StandardFonts } = use('pdf-lib');
var moment = use('moment')
const QRCode = use('qrcode')
const Papa = use('papaparse');
// const CSV = use('csv-parser');
const CsvToJson = use('csvtojson')
const { parseFile } = use('fast-csv');
// const axios = use('axios');
// MODULE

/**
 * Resourceful controller for interacting with projects
 */
class ProjectController {
  constructor() {
    this.res = new ResponseService
    this.dataHeader = new DataHeaderService
    this.base64 = new Base64Service
    this.redis = new RedisService
    this.pdf = new PdfService
    this.excel = new ExportService
    // this.checkDB = new CheckDbService
    // this.fetch = new ApiService
  }

  async dashboard ({ request, response, auth }) {
    try {
      await auth.check()
      let project = await Database
      .select(Database.raw('MONTHNAME(payments.updated_at) as name, SUM(master_skills.price) as total'))
      .from('payments')
      .innerJoin("master_skills", "payments.skill", "master_skills.skillName")
      .where((query) => {
        query.where('invoiceNumber', '>', 0)
        return query;
      })
      .groupByRaw("WEEK(payments.updated_at)")

      let monthArr = [
        { 'name': 'January', 'total': 0 },
        { 'name': 'February', 'total': 0 },
        { 'name': 'March', 'total': 0 },
        { 'name': 'April', 'total': 0 },
        { 'name': 'May', 'total': 0 },
        { 'name': 'June', 'total': 0 },
        { 'name': 'July', 'total': 0 },
        { 'name': 'August', 'total': 0 },
        { 'name': 'September', 'total': 0 },
        { 'name': 'October', 'total': 0 },
        { 'name': 'November', 'total': 0 },
        { 'name': 'December', 'total': 0 }
      ]

      var projectPayment = monthArr.map(obj => project.length > 0 && project.find(o => o.name === obj.name) || obj);

      let lineChart = {
        'label': monthArr.map(item => item.name),
        'projectPayment': projectPayment.map(item => item.total)
      }
      return response.status(200).json(await this.res.store('Get List Success', 200, 'SUCCESS', lineChart))
    } catch (error) {
      console.log('chart', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }

  async index ({ request, response, auth }) {
    try {
      await auth.check()
      let headerData = await this.dataHeader.getHeaderData(request)
      var { uid, current, filter, pageSize } = headerData
        var filterParams = {
          // name: filter ? (filter.type === 'name' ? filter.search : '') : '',
          // skill: filter ? (filter.type === 'skill' ? filter.search : '') : '',
          // userName: filter ? (filter.type === 'userName' ? filter.search : '') : '',
          // phoneNumber: filter ? (filter.type === 'phoneNumber' ? filter.search : '') : '',
          // nip: filter ? (filter.type === 'nip' ? filter.search : '') : '',
        }
      let project = Project.query().select('*', Database.raw(`JSON_CONTAINS(JSON_EXTRACT(projects.skill, '$**.*'), 'null', '$') AS skillValue, JSON_CONTAINS(JSON_EXTRACT(projects.size, '$**.value.*'), '0', '$') AS sizeValue`)).where('projectId', uid)
      .where((query) => {
        if(filter && filter.type === 'name' ? filter.search : ''){
          // query.whereRaw(`JSON_EXTRACT(projects.description,'$."NAMA"') LIKE "%${filter.search}%"`)
          query.where('projects.description', 'LIKE', `%${filter.search}%`)
        }
        // if(filter && filter.type === 'status'){
        //   if(filter && filter.orderBy === 'desc'){
        //     query
        //     .orderBy('skillValue', 'desc')
        //     .orderBy('sizeValue', 'desc')
        //   }else{
        //     query
        //     .orderBy('skillValue', 'asc')
        //     .orderBy('sizeValue', 'asc')
        //   }
        // }
        // if(filter && filter.type === 'size'){
        //   if(filter && filter.orderBy === 'desc'){
        //     query
        //     .orderBy('sizeValue', 'desc')
        //   }else{
        //     query
        //     .orderBy('sizeValue', 'asc')
        //   }
        // }
        // if(filter && filter.type === 'skill'){
        //   if(filter && filter.orderBy === 'desc'){
        //     query
        //     .orderBy('skillValue', 'desc')
        //   }else{
        //     query
        //     .orderBy('skillValue', 'asc')
        //   }
        // }
        // if(filter && filter.type === 'scan'){
          // if(filter && filter.orderBy === 'desc'){
          //   query
          //   .orderBy('currentScan', 'desc')
          // }else{
          //   query
          //   .orderBy('currentScan', 'asc')
          // }
        // }
        return query;
      })

      switch (filter && filter.type) {
        case 'status':
          if(filter && filter.orderBy === 'desc'){
            project
            .orderBy('skillValue', 'desc')
            .orderBy('sizeValue', 'desc')
          }else{
            project
            .orderBy('skillValue', 'asc')
            .orderBy('sizeValue', 'asc')
          }
          break;
        case 'size':
          if(filter && filter.orderBy === 'desc'){
            project
            .orderBy('sizeValue', 'desc')
          }else{
            project
            .orderBy('sizeValue', 'asc')
          }
          break;
        case 'skill':
          if(filter && filter.orderBy === 'desc'){
            project
            .orderBy('skillValue', 'desc')
          }else{
            project
            .orderBy('skillValue', 'asc')
          }
          break;
        case 'scan':
          if(filter && filter.orderBy === 'desc'){
            project
            .orderBy('currentScan', 'asc')
          }else{
            project
            .orderBy('currentScan', 'desc')
          }
          break;
        default:
          if(filter && filter.orderBy === 'desc'){
            project
            .orderBy('indexSort', 'desc')
          }else{
            project
            .orderBy('indexSort', 'asc')
          }
      }

      // if(filter && filter.type === 'status'){
      //   if(filter && filter.orderBy === 'desc'){
      //     project
      //     .orderBy('skillValue', 'desc')
      //     .orderBy('sizeValue', 'desc')
      //   }else{
      //     project
      //     .orderBy('skillValue', 'asc')
      //     .orderBy('sizeValue', 'asc')
      //   }
      // }

      // if(filter && filter.type === 'size'){
      //   if(filter && filter.orderBy === 'desc'){
      //     project
      //     .orderBy('sizeValue', 'desc')
      //   }else{
      //     project
      //     .orderBy('sizeValue', 'asc')
      //   }
      // }

      // if(filter && filter.type === 'skill'){
      //   if(filter && filter.orderBy === 'desc'){
      //     project
      //     .orderBy('skillValue', 'desc')
      //   }else{
      //     project
      //     .orderBy('skillValue', 'asc')
      //   }
      // }

      // if(filter && filter.type === 'scan'){
      //   if(filter && filter.orderBy === 'desc'){
      //     project
      //     .orderBy('currentScan', 'asc')
      //   }else{
      //     project
      //     .orderBy('currentScan', 'desc')
      //   }
      // }

      // if(filter && filter.type === 'noPot'){
      //   if(filter && filter.orderBy === 'desc'){
      //     project
      //     .orderBy('indexSort', 'desc')
      //   }else{
      //     project
      //     .orderBy('indexSort', 'asc')
      //   }
      // }

      // if(filter && filter.orderBy === 'status'){
      //   project
      //   .orderBy('skillValue', filter && filter.orderBy ? filter.orderBy : 'desc')
      //   .orderBy('sizeValue', 'asc')
      // }

      // switch (filter && filter.orderBy) {
      //   case 'statusDesc':
      //     project
      //     .orderBy('skillValue', 'desc')
      //     .orderBy('sizeValue', 'desc')
      //     break;
      //   case 'statusAsc':
      //     project
      //     .orderBy('skillValue', 'asc')
      //     .orderBy('sizeValue', 'asc')
      //     break;
      //   case 'skillDesc':
      //     project
      //     .orderBy('skillValue', 'desc')
      //   case 'skillAsc':
      //     project
      //     .orderBy('skillValue', 'asc')
      //     break;
      //   case 'sizeDesc':
      //     project
      //     .orderBy('sizeValue', 'desc')
      //     break;
      //   case 'sizeAsc':
      //     project
      //     .orderBy('sizeValue', 'asc')
      //     break;
      //   case 'scanDesc':
      //     project
      //     .orderBy('currentScan', 'desc')
      //     break;
      //   case 'scanAsc':
      //     project
      //     .orderBy('currentScan', 'asc')
      //     break;
      //   default:
      //     project
      //     .orderBy('indexSort', 'asc')
      //     break;
      // }
      let projectQuery = await project
      .filter(filterParams)
      // .orderBy('created_at', filter && filter.orderBy ? filter.orderBy : 'asc')
      // .orderBy('indexSort', 'asc')
      .paginate(current || 1, pageSize || 10)
      let queryJson = projectQuery.toJSON()
      queryJson.data.forEach(element => {
        let totalSkill = Object.keys(element.skill).length
        let totalSize = element.size.length
        element.size.forEach((item) => {
          let value = item.value
          let getSize = !Object.values(value).every(e => e > 0) ? 1 : 0
          element.statusSize = getSize
        })
        let statusSkill = this.res.hasNull(element.skill)
        element.name = element.description && element.description.NAMA
        element.isi = element.description && element.description.ISI
        element.noPot = element.description && element.description['NO POT']
        element.noSeri = element.description && element.description['NO SERI']
        element.totalSkill = totalSkill
        element.totalSize = totalSize
        element.statusSkill = statusSkill
        delete element.description
        delete element.size
        delete element.skill
      });
      return response.status(200).json(await this.res.store('Get List Success', 200, 'SUCCESS', queryJson))
    } catch (error) {
      console.log('list project', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }

  async uploadProject ({ request, response }) {
    try {
      const validationOptions = {
        size: '10mb',
        extnames: ['csv']
      };
      const imageFile = request.file('project', validationOptions);
      if(imageFile){
        let unique = await this.base64.random()
        let filePath = `project`
        let fileName = `project_${unique}.${imageFile.extname}`

        const isExist = await Drive.exists(`${filePath}/${fileName}`);
        if(isExist){
          await Drive.delete(`${filePath}/${fileName}`)
        }
        await imageFile.move(Helpers.tmpPath(`${filePath}`), {
          name: `project_${unique}.${imageFile.extname}`,
          overwrite: true,
        });

        if (!imageFile.moved()) {
          return response.status(200).json(await this.res.store('Only upload csv', 400, 'FAILED', imageFile.error()))
        }

        let tmp = Helpers.tmpPath(filePath)
        let pathTmp = `${tmp}/project_${unique}.${imageFile.extname}`

        // let jsonArray = await csvtojson({
        //   maxRowLength: 10,
        //   checkType:true,
        //   nullObject: true,
        // }).fromFile(pathTmp);

        let result = {
          // data: jsonArray,
          fileName: fileName
        }

        // jsonArray.forEach((item, index) => {
        //   item.indexProject = `project_${index}`
        //   Redis.rpush('qq', JSON.stringify(item))
        // })

        return response.status(200).json(await this.res.store('Upload Project Success', 200, 'SUCCESS', result))
      }
    } catch (error) {
      console.log('upload', error);
      return response.status(400).json(await this.res.store('Upload Error', 400, 'FAILED', error))
    }
  }

  async updateCachedProject ({ request, response, auth }) {
    try {
      let data = request.all()
      let { keyProject, indexProject } = data
      let dataProject = {...data}
      delete dataProject.keyProject
      delete dataProject.indexProject
      let project = await this.redis.update(keyProject, indexProject, dataProject)
      return response.status(200).json(await this.res.store('Update Project Success', 200, 'SUCCESS', project))
    } catch (error) {
      console.log('__update project', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }

  async updateSize ({ request, response, auth }) {
    try {
      await auth.check()
      let data = request.all()
      let project = await Project.find(data.uid)
      if(project){
        if(data.size){
          project.merge({
            size: JSON.stringify(data)
          })
        }else{
          project.merge({
            skill: JSON.stringify(data)
          })
        }
        await project.save()
        let msg = data.size ? 'Berhasil Mengubah Ukuran Project' : 'Berhasil Mengubah Skill Project'
        return response.status(200).json(await this.res.store(msg, 200, 'SUCCESS', project))
      }else{
        return response.status(200).json(await this.res.store('Project Tidak Ditemukan', 400, 'FAILED'))
      }
    } catch (error) {
      console.log('update size skill project', error);
      return response.status(400).json(await this.res.store('Terjadi Kesalahan', 400, 'FAILED', error))
    }
  }

  async document ({ request, response, auth }) {
    try {
      const data = request.all()
      const validationOptions = {
        size: '10mb',
        // extnames: ['csv']
      };
      const imageFile = request.file('file', validationOptions);
      if(imageFile){
        // let unique = await this.base64.random()
        let filePath = data.uid
        let fileName = `${data.operation}.${imageFile.extname}`

        const isExist = await Drive.exists(`${filePath}/${fileName}`);
        if(isExist){
          await Drive.delete(`${filePath}/${fileName}`)
        }
        await imageFile.move(Helpers.tmpPath(`${filePath}`), {
          name: fileName,
          overwrite: true,
        });

        if (!imageFile.moved()) {
          return response.status(200).json(await this.res.store('Error', 400, 'FAILED', imageFile.error()))
        }

        if(data.operation === 'project'){
          let project = await PlanningProject.find(data.uid)
          if(project){
            let pathTmp = Helpers.tmpPath(`${filePath}/${fileName}`)
            let jsonArray = await csvtojson({
              delimiter: 'auto',
              maxRowLength: 10,
              checkType:true,
              nullObject: true,
            }).fromFile(pathTmp);
            console.log('csv project', jsonArray)
            let totalProject = jsonArray.length
            let totalRange = project.total - project.totalProject
            if(totalProject > totalRange){
              return response.status(200).json(await this.res.store('File CSV Melebihi Total Project', 400, 'FAILED'))
            }else{
              let dataProject = await Project.createMany(jsonArray.map(item => {
                const sizePro = project.size.map(res => {
                  if(Array.isArray(res.value)){
                    res.value = res.value.reduce((a, b) => (a[b]=0, a), {})
                    return res
                  }
                  return res
                })
                let projectSize = JSON.stringify(sizePro)
                // let arrSkill = []
                let convertSkill = project.skill.reduce((a, b) => (a[b]=null, a), {})
                // arrSkill.push(convertSkill)
                let projectSkill = JSON.stringify(convertSkill)
                return{
                  description: JSON.stringify(item),
                  size: projectSize,
                  skill: projectSkill,
                  projectId: data.uid
                }
              }))
              let newProject = []
              for (const pro of dataProject) {
                const proJson = pro.toJSON()
                const descParse = JSON.parse(proJson.description)
                const name = descParse && descParse.NAMA ? descParse.NAMA : '-'
                const arrProject = [pro.uid, name]
                newProject.push(arrProject)
              }

              let newArray = newProject[Symbol.iterator]();
              let pdfProject =  Array.from({ length: Math.ceil(newProject.length / 10) }, _ => Array.from({ length: 10 }, _ => newArray.next().value ));
              let objPro = {
                uid: project.uid,
                name: project.name,
                project: pdfProject
              }
              await this.pdf.createPrintQr(objPro)
              // project.merge({ totalProject: totalProject + project.totalProject })
              // await project.save()
              await PlanningProject.query().where('uid', data.uid).update({ totalProject: totalProject + project.totalProject })
            }
          }else{
            return response.status(200).json(await this.res.store('Project Tidak Ditemukan', 400, 'FAILED'))
          }
        }

        return response.status(200).json(await this.res.store('Berhasil Upload Dokumen', 200, 'SUCCESS'))
      }else{
        return response.status(200).json(await this.res.store('Dokumen Tidak Ditemukan', 400, 'FAILED'))
      }
    } catch (error) {
      console.log('document', error);
      return response.status(400).json(await this.res.store('Terjadi Kesalahan', 400, 'FAILED', error))
    }
  }

  async storeProject ({ request, response, auth }) {
    try {
      await auth.check()
      let data = request.all()
      // let dataProject = {...data}
      // data.skill = JSON.stringify(data.skill)
      // data.size = JSON.stringify(data.size)
      const validationOptions = {
        size: '10mb',
        // extnames: ['csv']
      };
      const imageFile = request.file('file', validationOptions);
      if(imageFile){
        let filePath = data.name
        let fileName = `project.${imageFile.extname}`
        
        await imageFile.move(Helpers.tmpPath(`${filePath}`), {
          name: fileName,
          overwrite: true,
        });

        if (!imageFile.moved()) {
          return response.status(200).json(await this.res.store('Error', 400, 'FAILED', imageFile.error()))
        }
        let pathTmp = Helpers.tmpPath(`${filePath}/${fileName}`)
        let jsonArray = await csvtojson({
          delimiter: 'auto',
          maxRowLength: 10,
          checkType:true,
          nullObject: true,
        }).fromFile(pathTmp);
        console.log('store csv', jsonArray);
        let totalProject = jsonArray.length
        
        if(totalProject > data.total){
          return response.status(200).json(await this.res.store('File CSV Melebihi Total Project', 400, 'FAILED'))
        }

        let project = await PlanningProject.create(data)
        await project.reload()

        const isExist = await Drive.exists(`${filePath}/${fileName}`);
        if(isExist){
          await Drive.delete(`${filePath}/${fileName}`)
        }

        for (const [i, item] of jsonArray.entries()) {
          const sizePro = project.size.map(res => {
            if(Array.isArray(res.value)){
              res.value = res.value.reduce((a, b) => (a[b]=0, a), {})
              return res
            }
            return res
          })
          let projectSize = JSON.stringify(sizePro)
          // let arrSkill = []
          let convertSkill = project.skill.reduce((a, b) => (a[b]=null, a), {})
          // arrSkill.push(convertSkill)
          let projectSkill = JSON.stringify(convertSkill)
          await Project.create({ 
            description: JSON.stringify(item),
            size: projectSize,
            skill: projectSkill,
            projectId: project.uid,
            indexSort: i
          })
        }

        // let dataProject = await Project.createMany(jsonArray.map(item => {
        //   const sizePro = project.size.map(res => {
        //     if(Array.isArray(res.value)){
        //       res.value = res.value.reduce((a, b) => (a[b]=0, a), {})
        //       return res
        //     }
        //     return res
        //   })
        //   let projectSize = JSON.stringify(sizePro)
        //   // let arrSkill = []
        //   let convertSkill = project.skill.reduce((a, b) => (a[b]=null, a), {})
        //   // arrSkill.push(convertSkill)
        //   let projectSkill = JSON.stringify(convertSkill)
        //   return{
        //     description: JSON.stringify(item),
        //     size: projectSize,
        //     skill: projectSkill,
        //     projectId: project.uid
        //   }
        // }))

        // let newProject = []
        // for (const pro of dataProject) {
        //   const proJson = pro.toJSON()
        //   const descParse = JSON.parse(proJson.description)
        //   const name = descParse
        //   const arrProject = [pro.uid, name]
        //   const sizeParse = JSON.parse(proJson.size)
        //   const sizeHeader = sizeParse.map(item => Object.keys(item.value)).flat()
        //   const sizeValue = sizeParse.map(item => Object.values(item.value)).flat()
        //   const obj = {
        //     uid: proJson.uid,
        //     description: JSON.parse(proJson.description),
        //     header: sizeHeader,
        //     value: sizeValue,
        //     projectName: project.name
        //   }
        //   newProject.push(obj)
        // }

        // // let newArray = newProject[Symbol.iterator]();
        // // let pdfProject =  Array.from({ length: Math.ceil(newProject.length / 10) }, _ => Array.from({ length: 10 }, _ => newArray.next().value ));
        // // let objPro = {
        // //   uid: project.uid,
        // //   name: project.name,
        // //   project: pdfProject
        // // }
        // await this.pdf.printQrv2(newProject, project.uid)
        await PlanningProject.query().where('uid', project.uid).update({ totalProject: totalProject + project.totalProject })
        return response.status(200).json(await this.res.store('Berhasil Membuat Project', 200, 'SUCCESS', project))
      }else{
        return response.status(200).json(await this.res.store('Harap Unggah CSV', 400, 'FAILED'))
      }
    } catch (error) {
      console.log('store project', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }

  async storeQrCode ({ request, response, auth }) {
    try {
      await auth.check()
      var data = request.all()
      // var dataUid = {
      //   uid: data.uid
      // }
      var qq = await this.base64.encode(data.uid)
      return response.status(200).json(await this.res.store('Update Plan Project Success', 200, 'SUCCESS', qq))
    } catch (error) {
      console.log('update plan', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }

  async deleteProject ({ request, response, auth }) {
    try {
      await auth.check()
      const data = request.all()
      const project = await Project.query().with('planProject').where('uid', data.uid).where('currentScan', 0).first()
      if(project){
        let proJson = project.toJSON()
        await project.delete()
        const countProject = await Database.from('projects').where('projectId', proJson.planProject.uid).count('* as total')
        const total = countProject[0].total
        await project.planProject().update({ totalProject: total })
        return response.status(200).json(await this.res.store('Project Berhasil Dihapus', 200, 'SUCCESS'))
      }else{
        return response.status(200).json(await this.res.store('Project Gagal Dihapus', 400, 'FAILED'))
      }
    } catch (error) {
      console.log('delete project', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }

  async scanProject ({ request, response, auth }) {
    try {
      var user = await auth.getUser()
      var data = request.all();
      var project = await Project.query()
      .with('planProject', (builder) => {
        // builder.select('planning_projects.uid', 'planning_projects.count')
      })
      .where('uid', data.uid)
      .where('isScan', 0)
      // .select('projects.uid', 'projects.projectId', 'projects.isScan')
      .first()

      if(project){
        var projectJson = project.toJSON()
        var statusSkill = this.res.hasNull(projectJson.skill)
        if(statusSkill){
          return response.status(200).json(await this.res.store('Pemilihan Tukang Belum Lengkap', 400, 'FAILED'))
        }else{
          var _project = Object.entries(projectJson.skill)
          if(_project[projectJson.currentScan]){
            await Payment.create({
              userId: _project[projectJson.currentScan][1],
              skill: _project[projectJson.currentScan][0],
              projectId: projectJson.uid,
              planProjectId: projectJson.planProject.uid
            })
  
            await ProjectProcess.create({
              userId: _project[projectJson.currentScan][1],
              qcId: user.uid,
              skill: _project[projectJson.currentScan][0],
              // user: JSON.stringify([userProject[projectJson.currentScan]]),
              projectId: projectJson.uid
            })
            project.merge({ currentScan: project.currentScan + 1 })
            await project.save()
            await project.reload()
            if(_project.length == project.currentScan){
              await project.merge({ isScan: 1 })
              await project.save()
              await project.reload()
              await project.planProject().update({
                count: projectJson.planProject.count + 1
              })
            }
            return response.status(200).json(await this.res.store('Berhasil Scan Project', 200, 'SUCCESS'))
          }else{
            return response.status(200).json(await this.res.store('Project Sudah Selesai', 400, 'FAILED'))
          }
        }
      }else{
        return response.status(200).json(await this.res.store('Project Tidak Ditemukan / Sudah Discan', 404, 'FAILED'))
      }
    } catch (error) {
      console.log('scan project', error);
      return response.status(400).json(await this.res.store('Scan Project Failed', 400, 'FAILED', error))
    }
  }

  async updateProject ({ request, response, auth }) {
    try {
      await auth.check()
      var data = request.all()
      var project = await Project.find(data.uid)
      if(project){
        switch (data.type) {
          case 'size':
            var updateSize = {
              size: data.size
            }
            project.merge(updateSize)
            break;
          case 'skill':
            var updateSkill = {
              skill: data.skill
            }
            project.merge(updateSkill)
            break;
          default:
            var updateDescription = {
              description: data.description
            }
            project.merge(updateDescription)
            break;
        }
        await project.save()
        // var dataRequest = data.description ?
        // updateDescription :
        // data.size ?
        // updateSize :
        // updateSkill
        // var project = await Project.query().where('uid', data.uid).update(dataRequest)
        return response.status(200).json(await this.res.store('Berhasil Mengubah Project', 200, 'SUCCESS', project))
      }else{
        return response.status(200).json(await this.res.store('Project Tidak Ditemukan', 400, 'FAILED'))
      }
    } catch (error) {
      console.log('update plan', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }

  async showProject ({ params, request, response, auth }) {
    try {
      await auth.getUser()
      var headerData = await this.dataHeader.getHeaderData(request)
      var project = await Project.query()
      .where('uid', headerData.uid)
      // .where('isDeleted', '=', false)
      // .with('project', (builder) => {
      //   builder.with('sizeClothes').with('sizePants')
      // })
      .with('planProject')
      .with('process', (builder) => {
        builder
        .with('qc', (builder) => {
          builder.select('users.uid').with('userContact', (builder) => builder.select('user_contacts.uid', 'user_contacts.userId', 'user_contacts.fullName'))
        })
        .with('user', (builder) => {
          builder.select('users.uid').with('userContact', (builder) => builder.select('user_contacts.uid', 'user_contacts.userId', 'user_contacts.fullName'))
        })
        .orderBy('created_at', 'ASC')
      })
      .first()
      if(project){
        let projectJson = project.toJSON()
        let user = await User.query()
        .where('userType', 3)
        .where('isDeleted', 0)
        .rightJoin('skills', 'users.uid', 'skills.userId')
        .rightJoin('user_contacts', 'users.uid', 'user_contacts.userId')
        .select('users.uid', 'user_contacts.fullName', 'skills.skill', 'users.isStaff')
        .fetch()
        
        let userJson = user && user.toJSON()
        userJson.forEach(item => {
          item.skill = JSON.parse(item.skill)
        })

        let qcProject = projectJson.process.map(item => item.qc.userContact.fullName)
        let userProject = projectJson.process.map(item => item.user.userContact.fullName)
        let progress = Object.keys(projectJson.skill).map((item, i) => {
          return{
            "skill": item, 'qcName': qcProject[i], 'user': userProject[i], 'scanned': projectJson.currentScan > i
          }
        })

        projectJson.historyProcess = progress

        switch (headerData.type) {
          case 'description':
            return response.status(200).json(await this.res.store('Get List Success', 200, 'SUCCESS', projectJson.description))      
          case 'skill':
            let convertSkill = await Promise.all(Object.entries(projectJson.skill).map(async (item) => {
              let filterUser = userJson.filter(res => res.skill.find(e => e === item[0])).map(_item => {
                return {
                  value: _item.uid,
                  name: _item.fullName,
                  staff: _item.isStaff ? 'Staff' : 'Non Staff'
                }
              })
              var data = await User.query().with('userContact').where('isDeleted', 0).where('uid', item[1]).fetch();
              var ndata = data.toJSON();
              return {
                [item[0]]: {
                  value: ndata[0] ? {
                    name: ndata[0] && ndata[0].userContact.fullName,
                    staff: ndata[0] && ndata[0].isStaff ? 'Staff' : 'Non Staff',
                    value: item[1]
                  } : null,
                  user: filterUser
                }
              };
            }))
            return response.status(200).json(await this.res.store('Get List Success', 200, 'SUCCESS', convertSkill))
          case 'size':
          return response.status(200).json(await this.res.store('Get List Success', 200, 'SUCCESS', projectJson.size))  
          case 'process':
            let objProcess = {
              currentStatus: projectJson.currentScan,
              historyProcess: progress
            }
          return response.status(200).json(await this.res.store('Get List Success', 200, 'SUCCESS', objProcess))
          default:
            return response.status(200).json(await this.res.store('Get List Success', 200, 'SUCCESS', projectJson))        
        }
      }else{
        return response.status(200).json(await this.res.store('Project Tidak Ditemukan', 401, 'FAILED'))
      }
    } catch (error) {
      console.log('detail project', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }

  async changeStatusProject ({ request, response, auth }) {
    try {
      await auth.check()
      var data = request.all();
      var project = await PlanningProject.find(data.uid)
      if(project){
        project.merge({ isHold: data.isHold })
        await project.save()
        return response.status(200).json(await this.res.store('Berhasil Mengubah Status Project', 200, 'SUCCESS'))
      }else{
        return response.status(200).json(await this.res.store('Project Tidak Ditemukan', 404, 'FAILED'))
      }
    } catch (error) {
      console.log('change status', error);
      return response.status(400).json(await this.res.store('Change Status Project Failed', 400, 'FAILED', error))
    }
  }

  async printQR ({ request, response, auth }) {
    try {    
      await auth.check()
      let data = request.all()
      let project = await PlanningProject.query()
      .where('uid', data.uid)
      .with('project', (builder) => {
        builder.offset(data.from - 1).limit(data.to - (data.from - 1)).orderBy('indexSort', 'asc')
      })
      .first()

      const tmp = Helpers.tmpPath()
      const fileName = `${project.toJSON().name} ${data.part}`
      const isExist = await Drive.exists(tmp + '/' + fileName.split(' ').join('-') + '.pdf');
      let file = `http://cdn.tempa-bersama.co.id/${fileName.split(' ').join('-')}.pdf`
      if(isExist){
        await Drive.delete(tmp + '/' + fileName.split(' ').join('-') + '.pdf')
      }

      // if(isExist){
      //   return response.status(200).json(await this.res.store('Donwload Document Success', 200, 'SUCCESS', file))
      // }else{
        let newProject = []
        for (const proJson of project.toJSON().project) {
          // const sizeParse = JSON.parse(proJson.size)
          const sizeHeader = proJson.size.map(item => Object.keys(item.value)).flat()
          const sizeValue = proJson.size.map(item => Object.values(item.value)).flat()
          const obj = {
            uid: proJson.uid,
            description: proJson.description,
            header: sizeHeader,
            value: sizeValue,
            projectName: project.name
          }
          newProject.push(obj)
        }

        // let newArray = newProject[Symbol.iterator]();
        // let pdfProject =  Array.from({ length: Math.ceil(newProject.length / 10) }, _ => Array.from({ length: 10 }, _ => newArray.next().value ));
        // let objPro = {
        //   uid: project.uid,
        //   name: project.name,
        //   project: pdfProject
        // }
        await this.pdf.printQrv2(newProject, project.uid, fileName)
        return response.status(200).json(await this.res.store('Donwload Document Success', 200, 'SUCCESS', file))
      // }

      // console.log(123, pro.toJSON());

      // let file = `http://cdn.tempa-bersama.co.id/${data.uid}.pdf`

      // let aa = pro.toJSON().project.map(item => {
      //   return [item.uid, item.description.NAMA]
      // })
      // var newArray = aa[Symbol.iterator]();
      // var gg =  Array.from({ length: Math.ceil(aa.length / 10) }, _ => Array.from({ length: 10 }, _ => newArray.next().value ));
      // let pdf = await this.pdf.createInvoice(gg)
      // return response.download(pathName)
      // return response.status(200).json(await this.res.store('Donwload Document Success', 200, 'SUCCESS', pro))
    } catch (error) {
      console.log('pdf', error);
      return response.status(400).json(await this.res.store('Donwload Document Failed', 400, 'FAILED', error))
    }
  }

  async printProject ({ request, response, auth }) {
    try {    
      await auth.check()
      let data = request.all()
      let project = await PlanningProject.query()
      .select('planning_projects.uid', 'planning_projects.name')
      .where('uid', data.uid)
      // .with('project')
      .first()

      let itemProject = await Project.query()
      .whereIn('uid', data.project)
      .orderBy('indexSort', 'asc')
      .fetch()

      if(project){
        let proJson = project.toJSON()
        let itemProjectJson = itemProject.toJSON()
        let newArr = []
        for (const item of itemProjectJson) {
          let skill = Object.values(item.skill)
          let skillProject = Object.keys(item.skill)
          let user = await User.query()
          .select('users.uid')
          .with('userContact', (builder) => {
            builder.select('user_contacts.uid', 'user_contacts.userId', 'user_contacts.fullName')
          })
          .whereIn('users.uid', skill)
          .fetch()

          let checkSkill = skill.some(item => item !== null)

          if(checkSkill){
            let userProject = skill.map((obj) => user.toJSON().find(o => o.uid === obj)).map(item => item.userContact.fullName)
            let result = []
            for ( var i = 0; i < skillProject.length; i++ ) {
              result.push( [ skillProject[i], userProject[i] ] );
            }
            let obj = {
              name: proJson.name,
              desc: item.description,
              listUser: result
            }
            newArr.push(obj)
          }else{
            return response.status(200).json(await this.res.store('Pemilihan Tukang Belum Lengkap', 400, 'FAILED'))
          }
        }

        const tmp = Helpers.tmpPath()
        const isExist = await Drive.exists(tmp + '/' + data.uid + '.pdf');
        let file = `http://cdn.tempa-bersama.co.id/${data.uid}_project.pdf`
        if(isExist){
          await Drive.delete(tmp + '/' + data.uid + '.pdf')
        }
        await this.pdf.printProject(newArr, proJson.uid)
        return response.status(200).json(await this.res.store('Print Project Success', 200, 'SUCCESS', file))
      }else{
        return response.status(200).json(await this.res.store('Project Tidak Ditemukan', 400, 'FAILED'))
      }
    } catch (error) {
      console.log('print project', error);
      return response.status(400).json(await this.res.store('Terjadi Kesalahan', 400, 'FAILED', error))
    }
  }

  async storeComment ({ request, response, auth }) {
    try {
      const user = await auth.getUser()
      const data = request.all()
      const comment = await ProjectComment.create({
        userId: user.uid, 
        planProjectId: data.planProjectId,
        projectId: data.projectId,
        notes: data.notes
      })
      return response.status(200).json(await this.res.store('Berhasil Membuat Komentar Diproject', 200, 'SUCCESS', comment))
    } catch (error) {
      console.log('create comment project', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }

  async deleteComment ({ request, response, auth }) {
    try {
      await auth.check()
      const data = request.all()
      const comment = await ProjectComment.find(data.id)
      if(comment){
        await comment.delete()
        return response.status(200).json(await this.res.store('Berhasil Delete Komentar', 200, 'SUCCESS'))
      }else{
        return response.status(200).json(await this.res.store('Komentar Tidak Ditemukan', 400, 'FAILED'))
      }
    } catch (error) {
      console.log('create comment project', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }
}

module.exports = ProjectController
