'use strict'

// SERVICES
const ResponseService = use('App/Services/ResponseService');
// const CheckToken = use('App//Middleware/CheckToken');
const DataHeaderService = use('App/Services/DataHeaderService');
// const CheckDbService = use('App/Services/CheckDbService');
// const ApiService = use('App/Services/ApiService');
// const Base64Service = use('App/Services/Base64Service');
// SERVICES

// MODELS
const User = use('App/Models/User')
const UserContact = use('App/Models/UserContact')
const Skill = use('App/Models/Skill')
const MasterSkill = use('App/Models/MasterSkill')
const Payment = use('App/Models/Payment')
// const Verification = use('App/Models/UserModel/UserVerification')
// const Type = use('App/Models/UserType')
// const FaceRecog = use('App/Models/UserModel/UserFaceReq')
// const UserSIM = use('App/Models/SIM/UserSim')
// const Token = use('App/Models/Token')
// const Regencie = use('App/Models/Regency')
// const Provincy = use('App/Models/Provincy')
// const District = use('App/Models/District')
// const OutletContact = use('App/Models/OutletContact')
// const DoctorSchedule = use('App/Models/Doctor/DoctorSchedule')
// const Counseling = use('App/Models/Counseling')
// const DoctorContact = use('App/Models/Doctor/DoctorContact')
// const File = use('App/Models/File')
// const SIMCategory = use('App/Models/SimCategory')
// MODELS

//ADONIS
const Database = use('Database');

/**
 * Resourceful controller for interacting with users
 */
class UserController {
  constructor() {
    this.res = new ResponseService
    this.dataHeader = new DataHeaderService
    // this.base64 = new Base64Service
    // this.checkDB = new CheckDbService
    // this.fetch = new ApiService
  }

  async index ({ request, response, auth }) {
    try {
      await auth.getUser()
      var headerData = await this.dataHeader.getHeaderData(request)
      var { userType, current, filter, pageSize } = headerData
      var filterParams = {
        fullName: filter ? (filter.type === 'fullName' ? filter.search : '') : '',
        skill: filter ? (filter.type === 'skill' ? filter.search : '') : '',
        userName: filter ? (filter.type === 'userName' ? filter.search : '') : '',
        phoneNumber: filter ? (filter.type === 'phoneNumber' ? filter.search : '') : '',
        nip: filter ? (filter.type === 'nip' ? filter.search : '') : '',
      }
      var user = User.query()
      .whereNotIn('userType', [1])
      .where('isDeleted', '=', false)
      switch (userType) {
        case 2:
          user
          .where('userType', userType)
          .with('userContact')
          .with('role')
          break;
        case 3:
          user
          .where('userType', userType)
          .with('userContact')
          .with('role')
          .with('skill')
          .with('payment')
          break;
        case 4:
          user
          .where('userType', userType)
          .with('userContact')
          .with('role')
          break;
        case 5:
          user
          .where('userType', userType)
          .with('userContact')
          .with('role')
          break;
        case 6:
          user
          .where('userType', userType)
          .with('userContact')
          .with('role')
          break;
        default:
          user
          .with('userContact')
          .with('role')
          .with('skill')
          break;
      }
      var userQuery = await user
      .filter(filterParams)
      .orderBy(`created_at`, filter && filter.orderBy ? filter.orderBy : 'desc')
      .paginate(current || 1, pageSize || 10)
      var masterSkill = await MasterSkill.all()
      var skillJson = masterSkill.toJSON()
      var userJson = userQuery.toJSON()
      userJson.data.forEach(item => {
        if(item.skill){
          // item.skill.skill = JSON.parse(item.skill.skill)
          var totalPrie = skillJson.filter(res => item.skill.skill.includes(res.skillName)).map(res => res.price).reduce((a, b) => a + b, 0)
          item.skill.totalPrie = totalPrie
        }
      })
      return response.status(200).json(await this.res.store('Get List Success', 200, 'SUCCESS', userJson))
    } catch (error) {
      console.log('list', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }

  async store ({request, response, auth}) {
    try {
      await auth.check()
      var data = request.all();
      var checkPhone = await User.findBy('phoneNumber', data.phoneNumber)
      if(!checkPhone){
        if(data.userName){
          var checkUsername = await User.findBy('userName', data.userName)
          if(checkUsername) return response.status(200).json(await this.res.store('Username Sudah Digunakan', 406, 'FAILED'))
        }
        var user = await User.create({
          userName: data.userName,
          password: data.password,
          nip: data.nip,
          phoneNumber: data.phoneNumber,
          userType: data.type,
          isStaff: data.isStaff
        })
        await user.reload();
        await user.userContact().create({
          userId: user.uid,
          fullName: data.fullName,
          address: data.address,
          gender: data.gender,
        });
        if(data.type === 3){
          const skills = JSON.stringify(data.skills)
          await user.skill().create({
            userId: user.uid,
            skill: skills
          });
          await user.load('skill')
        }
        await user.load('userContact')
        return response.status(200).json(await this.res.store('Berhasil Membuat User', 200, 'SUCCESS', user))
      }else{
        return response.status(200).json(await this.res.store('Nomor Telepon Sudah Digunakan', 406, 'FAILED'))
      }
    } catch (error) {
      console.log('create', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }

  async update ({ request, response, auth }) {
    try {
      var data = request.all()
      var authUser = await auth.getUser()
      var isUid = data.uid ? data.uid : authUser.uid
      var user = await User.find(isUid)
      if(user){
        user.merge({
          userName: data.userName,
          password: data.password,
          nip: data.nip,
          phoneNumber: data.phoneNumber,
          isStaff: data.isStaff
        })
        await user.save()
        await user.reload()
        await UserContact.query().where('userId', user.uid).update({
          fullName: data.fullName,
          address: data.address,
          gender: data.gender,
        })
        if(user.userType === 3){
          const skills = JSON.stringify(data.skills)
          // var userSkill = await Skill.query().where('userId', user.uid).first()
          // var skillName = userSkill.toJSON().skill
          // var addSkill = data.skills.filter(item => skillName.indexOf(item) === -1)
          // var deleteSkill = skillName.filter(item => data.skills.indexOf(item) === -1)
          // console.log(111, addSkill);
          // console.log(222, deleteSkill);
          // if(addSkill.length > 0){
          //   for (let item of addSkill){
          //     await Database.raw(`UPDATE projects SET skill = JSON_REPLACE(skill, '$."${item}"', null) WHERE JSON_SEARCH(skill, 'all', "${user.uid}") IS NOT NULL`)
          //   }
          // }

          // if(deleteSkill.length > 0){
          //   for (let item of deleteSkill){
          //     await Database.raw(`UPDATE projects SET skill = JSON_REPLACE(skill, '$."${item}"', null) WHERE JSON_SEARCH(skill, 'all', "${user.uid}") IS NOT NULL`)
          //   }
          // }
          await Skill.query().where('userId', user.uid).update({
            skill: skills
          })
        }
        await user.reload()
        await user.loadMany(['userContact', 'skill'])
        return response.status(200).json(await this.res.store('Berhasil Mengubah User', 200, 'SUCCESS', user))
      }else{
        return response.status(200).json(await this.res.store('User Tidak Ditemukan', 404, 'FAILED'))
      }
    } catch (error) {
      console.log('update', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }

  async show ({ request, response, auth }) {
    try {
      var headerData = await this.dataHeader.getHeaderData(request)
      var authUser = await auth.getUser()
      var isUid = headerData && headerData.uid ? headerData.uid : authUser.uid
      var { current, pageSize, filter } = headerData
      var user = await User.query()
      .where('isDeleted', '=', false)
      .where('uid', isUid)
      .with('userContact')
      .with('role')
      .with('skill')
      .with('payment', (builder) => {
        builder.select('*', Database.raw(`SUM(master_skills.price) as total, COUNT(payments.skill) as totalSkill`)).with('user').with('project', (builder) => {
          builder.select('projects.uid', 'projects.projectId').with('planProject', (builder) => {
            builder.select('planning_projects.uid', 'planning_projects.name')
          })
        })
        .leftJoin('master_skills', 'payments.skill', 'master_skills.skillName')
        .groupBy('payments.skill')
      })
      .first()

      var filterParams = {
        // name: filter ? (filter.type === 'name' ? filter.search : '') : '',
        // date: filter ? (filter.type === 'date' ? filter.search : '') : '',
        // fullName: filter ? (filter.type === 'fullName' ? filter.search : '') : '',
        // skill: filter ? (filter.type === 'skill' ? filter.search : '') : '',
        // userName: filter ? (filter.type === 'userName' ? filter.search : '') : '',
        // phoneNumber: filter ? (filter.type === 'phoneNumber' ? filter.search : '') : '',
        // nip: filter ? (filter.type === 'nip' ? filter.search : '') : '',
      }

      var paymentHistory = await Payment.query()
      .select('*', 'payments.created_at as createPay', 'payments.updated_at as updatePay',  Database.raw(`SUM(master_skills.price) as total, COUNT(payments.skill) as totalSkill, IF(payments.invoiceNumber = 0, null, DATE_FORMAT(payments.updated_at, '%Y-%m-%d')) as datePay`))
      .with('user')
      .with('project', (builder) => {
        builder.select('projects.uid', 'projects.projectId').with('planProject', (builder) => {
          builder.select('planning_projects.uid', 'planning_projects.name')
        })
      })
      .where((query) => {
        if(filter && filter.type === 'date' ? filter.search : ''){
          query.where('invoiceNumber', '>', 0).whereRaw(`DATE(payments.updated_at) = "${filter.search}"`)
        }
        return query;
      })
      .where('payments.userId', isUid)
      .leftJoin('master_skills', 'payments.skill', 'master_skills.skillName')
      .filter(filterParams)
      .groupBy('payments.skill')
      // .orderBy('payments.created_at', filter && filter.orderBy ? filter.orderBy : 'asc')
      .paginate(current || 1, pageSize || 10)

      let resPayment = {
        listPayment: paymentHistory && paymentHistory.toJSON()
      }
      let mergeRes = Object.assign(user.toJSON(), resPayment)
      
      if(user){
        return response.status(200).json(await this.res.store('Get Detail Success', 200, 'SUCCESS', mergeRes))
      }else{
        return response.status(200).json(await this.res.store('User Tidak Ditemukan', 404, 'FAILED'))
      }
      
    } catch (error) {
      console.log('detail', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }

  async destroy ({ request, response, auth }) {
    try {
      var data =  request.all();
      var authUser = await auth.getUser()
      var isUid = data && data.uid ? data.uid : authUser.uid
      var user =  await User.find(isUid)
      if(user){
        // user.merge({ isDeleted: 1 })
        // await user.save()
        await user.delete()
        return response.status(200).json(await this.res.store('Berhasil Menghapus User', 200, 'SUCCESS'))
      }else{
        return response.status(200).json(await this.res.store('User Tidak Ditemukan', 404, 'FAILED'))
      }
    } catch (error) {
      console.log('delete', error);
      return response.status(400).json(await this.res.store('Delete User Failed', 400, 'FAILED', error))
    }
  }

}

module.exports = UserController
