'use strict'


// SERVICES
const ResponseService = use('App/Services/ResponseService');
const DataHeaderService = use('App/Services/DataHeaderService');
const PdfService = use("App/Services/PdfService");
const ExportService = use("App/Services/ExportService");
// SERVICES

// MODELS
const User = use('App/Models/User')
const UserContact = use('App/Models/UserContact')
const Skill = use('App/Models/Skill')
const MasterSkill = use('App/Models/MasterSkill')
const Project = use("App/Models/Project");
const PlanningProject = use("App/Models/PlanningProject");
const Payment = use("App/Models/Payment");
// MODELS

// ADONIS
const Database = use('Database')
const Event = use('Event')
const Helpers = use('Helpers')
const Drive = use('Drive')
const Env = use('Env')
const fs = use('fs')
const csv = use('csv-parser');
const Redis = use('Redis')
// ADONIS

class PaymentController {
  constructor() {
    this.res = new ResponseService
    this.dataHeader = new DataHeaderService
    this.pdf = new PdfService
    this.excel = new ExportService
  }

  async index ({ request, view, response, auth }){
    try {
      var headerData = await this.dataHeader.getHeaderData(request)
      var { current, filter, pageSize } = headerData
      var filterParams = {
        name: filter ? (filter.type === 'name' ? filter.search : '') : '',
      }

      let userPayment = Payment.query()
      .select('payments.planProjectId', 'payments.invoiceNumber', Database.raw('sum(price) as totalPrice'))
      .with('planProject', (builder) => {
        builder.select('planning_projects.uid', 'planning_projects.name')
      })
      .where('invoiceNumber', 0)
      .innerJoin('master_skills', 'payments.skill', 'master_skills.skillName')
      .groupBy('planProjectId')

      var payQuery = await userPayment
      .filter(filterParams)
      .orderBy(`payments.created_at`, filter && filter.orderBy ? filter.orderBy : 'desc')
      // .paginate(current || 1, pageSize || 10)
      .fetch()

      var payPaginator = payQuery.rows.length > 0 ? this.res.paginator(payQuery.toJSON(), current || 1, pageSize || 10) : []

      return response.status(200).json(await this.res.store('Get List Success', 200, 'SUCCESS', payPaginator))
    } catch (error) {
      console.log('list payment', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }

  async show ({ request, view, response, auth }){
    try {
      var headerData = await this.dataHeader.getHeaderData(request)
      var payQuery = await Payment.query()
      .select('master_skills.price' ,'payments.skill', 'payments.invoiceNumber', 'payments.invoicePath', 'payments.planProjectId', 'payments.userId', Database.raw(`COUNT(*) as totalSkill, (master_skills.price * COUNT(*)) as totalPrice`))
      .where('payments.planProjectId', headerData.uid)
      .where('payments.invoiceNumber', 0)
      .with('planProject', (builder) => {
        builder.select('planning_projects.uid', 'planning_projects.name')
      })
      .with('user', (builder) => {
        builder.select('users.uid', 'users.nip', 'user_contacts.fullName').leftJoin('user_contacts', 'users.uid', 'user_contacts.userId')
      })
      .innerJoin('master_skills', 'payments.skill', 'master_skills.skillName')
      .innerJoin('users', 'payments.userId', 'users.uid')
      .groupBy('payments.skill')
      .fetch()

      return response.status(200).json(await this.res.store('Get List Success', 200, 'SUCCESS', payQuery))
    } catch (error) {
      console.log('detail payment', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }

  async listPay ({ request, response, auth }){
    try {
      var headerData = await this.dataHeader.getHeaderData(request)
      var { userType, current, filter, pageSize } = headerData
      var filterParams = {
        // name: filter ? (filter.type === 'name' ? filter.search : '') : '',
        // skill: filter ? (filter.type === 'skill' ? filter.search : '') : '',
        // userName: filter ? (filter.type === 'userName' ? filter.search : '') : '',
        // phoneNumber: filter ? (filter.type === 'phoneNumber' ? filter.search : '') : '',
        // nip: filter ? (filter.type === 'nip' ? filter.search : '') : '',
      }

      let result = Invoice.query()
      .with('user', (builder) => {
        builder
        .select('users.uid', 'users.nip', 'user_contacts.fullName')
        .innerJoin('user_contacts', 'users.uid', 'user_contacts.userId')
      })
      .with('payment', (builder) => {
        builder
        .select('payments.invoiceId', Database.raw('sum(price) as totalPrice'))
        .innerJoin('master_skills', 'payments.skill', 'master_skills.skillName')
        .groupBy('payments.invoiceId')
      })
      // .fetch()
      var planQuery = await result
      .filter(filterParams)
      .orderBy(`invoices.created_at`, filter && filter.orderBy ? filter.orderBy : 'desc')
      .paginate(current || 1, pageSize || 10)
      return response.status(200).json(await this.res.store('Get List Success', 200, 'SUCCESS', planQuery))
    } catch (error) {
      console.log('list payment', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }

  async showPay ({ request, response, auth }){
    try {
      var headerData = await this.dataHeader.getHeaderData(request)
      let pay = await Invoice.query().where('id', headerData.uid).with('user').with('payment').first()

      if(!pay){
        return response.status(200).json(await this.res.store('User Tidak Ditemukan', 400, 'FAILED'))
      }

      return response.status(200).json(await this.res.store('Get List Success', 200, 'SUCCESS', pay))
    } catch (error) {
      console.log('list', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }

  async pdfInvoice ({ request, response, auth }){
    try {
      var data = request.all()
      var headerData = await this.dataHeader.getHeaderData(request)
      let user = await User.query()
      .where('users.uid', data.uid)
      .innerJoin('user_contacts', 'users.uid', 'user_contacts.userId')
      .select('users.uid', 'user_contacts.fullName', 'users.nip', 'users.phoneNumber')
      .first()

      let payment = await Payment.query()
      .with('project', (builder) => {
        builder
        .with('planProject')
        .select('projects.uid', 'projects.projectId')
        // .join('planning_projects', 'projects.projectId', 'planning_projects.uid')
      })
      .leftJoin('master_skills', 'payments.skill', 'master_skills.skillName')
      .where('payments.userId', data.uid)
      // .where('payments.isPaid', 0)
      // .where(Database.raw('payments.created_at > DATE_SUB(CURDATE(), INTERVAL 1 WEEK)'))
      .fetch()
      let result = {
        user: user.toJSON(),
        project: payment.toJSON()
      }

      // console.log(123, result);
      // const invoice = {
      //   shipping: {
      //     name: "John Doe",
      //     address: "1234 Main Street",
      //     city: "San Francisco",
      //     state: "CA",
      //     country: "US",
      //     postal_code: 94111
      //   },
      //   items: [
      //     {
      //       item: "TC 100",
      //       description: "Toner Cartridge",
      //       quantity: 2,
      //       amount: 6000
      //     },
      //     {
      //       item: "USB_EXT",
      //       description: "USB Cable Extender",
      //       quantity: 1,
      //       amount: 2000
      //     }
      //   ],
      //   subtotal: 8000,
      //   paid: 0,
      //   invoice_nr: 1234
      // };
      let pdf = await this.pdf.createInvoice(result)
      // return pdf
      return response.status(200).json(await this.res.store('Get List Success', 200, 'SUCCESS', pdf))
    } catch (error) {
      console.log('pdf invoice', error);
    }
  }

  async payment ({ request, response, auth }){
    try {
      let data = request.all()
      let max = await Payment.query().getMax('invoiceNumber')

      let userPayment = await Payment.query()
      .where('planProjectId', data.uid)
      .where('invoiceNumber', null)
      .first()

      if(userPayment){
        if(max){
          await Payment.query()
          .where('planProjectId', data.uid)
          .where('invoiceNumber', null)
          .update({ invoiceNumber: max + 1})
        }else{
          await Payment.query()
          .where('planProjectId', data.uid)
          .where('invoiceNumber', null)
          .update({ invoiceNumber: 1})
        }
        return response.status(200).json(await this.res.store('Berhasil Melakukan Pembayaran', 200, 'SUCCESS', userPayment))
      }else{
        return response.status(200).json(await this.res.store('Project Tidak Ditemukan', 400, 'FAILED'))
      }
    } catch (error) {
      console.log(444, error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }

  async listPayment ({ request, response, auth }){
    try {
      // let data = request.all()
      var headerData = await this.dataHeader.getHeaderData(request)
      var { userType, current, filter, pageSize } = headerData
      var filterParams = {
        // name: filter ? (filter.type === 'name' ? filter.search : '') : '',
        // skill: filter ? (filter.type === 'skill' ? filter.search : '') : '',
        // userName: filter ? (filter.type === 'userName' ? filter.search : '') : '',
        // phoneNumber: filter ? (filter.type === 'phoneNumber' ? filter.search : '') : '',
        // nip: filter ? (filter.type === 'nip' ? filter.search : '') : '',
      }
      let userPayment = Payment.query()
      // .where('planProjectId', data.uid)
      .select('payments.planProjectId', Database.raw('sum(price) as totalPrice'))
      .with('planProject', (builder) => {
        // builder.select('planning_projetcs.uid', 'planning_projetcs.name')
      })
      // .where('isPaid', 0)
      .innerJoin('master_skills', 'payments.skill', 'master_skills.skillName')
      // .groupBy('planProjectId')
      // .fetch()

      var planQuery = await userPayment
      .filter(filterParams)
      .orderBy(`payments.created_at`, filter && filter.orderBy ? filter.orderBy : 'desc')
      .groupBy('planProjectId')
      .paginate(current || 1, pageSize || 10)

      // let pay = await Invoice.create({ 
      // })
      // await pay.reload()
      // await Payment.query()
      // .where('planProjectId', data.uid)
      // .where('isPaid', 0)
      // .update({
      //   invoiceId: pay.id,
      //   isPaid: 1
      // })
      return response.status(200).json(await this.res.store('Berhasil Melakukan Pembayaran', 200, 'SUCCESS', planQuery))
    } catch (error) {
      console.log(444, error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }

  async exportPayment ({ request, response, auth }) {
    try {
      let data = request.all()    
      // let project = await Invoice.query()
      // .with('user', (builder) => {
      //   builder
      //   .select('users.uid', 'users.nip', 'user_contacts.fullName')
      //   .innerJoin('user_contacts', 'users.uid', 'user_contacts.userId')
      // })
      // .with('payment', (builder) => {
      //   builder
      //   .select('payments.invoiceId', Database.raw('sum(price) as totalPrice'))
      //   .innerJoin('master_skills', 'payments.skill', 'master_skills.skillName')
      //   .groupBy('payments.invoiceId')
      // })
      // .fetch()

      // let project = await Database.raw(`SELECT payments.projectId, payments.planProjectId, planning_projects.name, payments.skill, payments.created_at, SUM(price) as totalPrice FROM payments INNER JOIN master_skills on payments.skill = master_skills.skillName INNER JOIN planning_projects on payments.planProjectId = planning_projects.uid WHERE payments.planProjectId = "${data.uid}" AND payments.isPaid = 0`)
      let project = await Database.raw(`SELECT payments.projectId, payments.planProjectId, planning_projects.name, planning_projects.companyName, payments.skill, ( SELECT created_at FROM payments WHERE payments.planProjectId = "${data.uid}" AND payments.invoiceNumber = 0 ORDER BY created_at ASC LIMIT 1 ) as dateFrom, SUM(price) as totalPrice FROM payments INNER JOIN master_skills on payments.skill = master_skills.skillName INNER JOIN planning_projects on payments.planProjectId = planning_projects.uid INNER JOIN users on payments.userId = users.uid WHERE payments.planProjectId = "${data.uid}" AND payments.invoiceNumber = 0 AND users.isStaff = 0`)
      let project3 = await Database.raw(`SELECT payments.projectId, payments.planProjectId, planning_projects.name,planning_projects.companyName, payments.skill, ( SELECT created_at FROM payments WHERE payments.planProjectId = "${data.uid}" AND payments.invoiceNumber = 0 ORDER BY created_at ASC LIMIT 1 ) as dateFrom, master_skills.price, SUM(CASE WHEN u.isStaff = 0 THEN master_skills.price ELSE 0 END) as totalPrice, user_contacts.fullName, u.nip, u.isStaff FROM payments INNER JOIN master_skills on payments.skill = master_skills.skillName INNER JOIN planning_projects on payments.planProjectId = planning_projects.uid INNER JOIN projects on payments.projectId = projects.uid LEFT JOIN users u ON payments.userId = u.uid LEFT JOIN user_contacts ON u.uid = user_contacts.userId WHERE payments.planProjectId = "${data.uid}" AND payments.invoiceNumber = 0 GROUP BY(user_contacts.fullName)`)
      // let project2 = await Database.raw(`SELECT payments.skill, ( SELECT COUNT(*) FROM projects WHERE projects.projectId = payments.planProjectId ) AS totalOrder, master_skills.price AS perUnit, ( SELECT master_skills.price * totalOrder ) AS total, COUNT(*) AS totalSkill, (CASE WHEN users.isStaff = 0 THEN master_skills.price ELSE 0 END * COUNT(*)) AS totalPrice, ( SELECT totalOrder - COUNT(*) FROM payments INNER JOIN master_skills ON payments.skill = master_skills.skillName WHERE payments.planProjectId = "${data.uid}" AND payments.invoiceNumber = 0 GROUP BY payments.skill LIMIT 1 ) AS sisaOrder, ( SELECT total - (master_skills.price * COUNT(master_skills.skillName)) ) AS sisaPrice FROM payments INNER JOIN master_skills ON payments.skill = master_skills.skillName WHERE payments.planProjectId = "${data.uid}" AND payments.invoiceNumber = 0 GROUP BY payments.skill`)
      
      // oke sih ini 
      // let project2 = await Database.raw(`SELECT payments.skill, ( SELECT COUNT(*) FROM projects WHERE projects.projectId = payments.planProjectId ) AS totalOrder, master_skills.price AS perUnit, ( SELECT master_skills.price * totalOrder ) AS total, COUNT(*) AS totalSkill, (CASE WHEN users.isStaff = 0 THEN master_skills.price ELSE 0 END * COUNT(*)) AS totalPrice, history.totalHistory + COUNT(*) as komulatifTotal, history.totalPriceHistory + (CASE WHEN users.isStaff = 0 THEN master_skills.price ELSE 0 END * COUNT(*)) as komulatifPrice FROM payments INNER JOIN master_skills ON payments.skill = master_skills.skillName LEFT JOIN ( SELECT payments.skill, COUNT(*) AS totalHistory, (CASE WHEN users.isStaff = 0 THEN master_skills.price ELSE 0 END * COUNT(*)) AS totalPriceHistory FROM payments INNER JOIN master_skills ON payments.skill = master_skills.skillName INNER JOIN users ON payments.userId = users.uid WHERE payments.invoiceNumber > 0 AND payments.planProjectId = "${data.uid}" GROUP BY payments.skill) as history ON payments.skill = history.skill INNER JOIN users ON payments.userId = users.uid WHERE payments.invoiceNumber = 0 AND payments.planProjectId = "${data.uid}" GROUP BY payments.skill`)
      let project2 = await Database.raw(`SELECT payments.skill, ( SELECT COUNT(*) FROM projects WHERE projects.projectId = payments.planProjectId ) AS totalOrder, master_skills.price AS perUnit, ( SELECT master_skills.price * totalOrder ) AS total, COUNT(*) AS totalSkill, (CASE WHEN users.isStaff = 0 THEN master_skills.price ELSE master_skills.price END * COUNT(*)) AS totalPrice, history.totalHistory + COUNT(*) as komulatifTotal, history.totalPriceHistory + (CASE WHEN users.isStaff = 0 THEN master_skills.price ELSE 0 END * COUNT(*)) as komulatifPrice FROM payments INNER JOIN master_skills ON payments.skill = master_skills.skillName LEFT JOIN ( SELECT payments.skill, COUNT(*) AS totalHistory, (CASE WHEN users.isStaff = 0 THEN master_skills.price ELSE 0 END * COUNT(*)) AS totalPriceHistory FROM payments INNER JOIN master_skills ON payments.skill = master_skills.skillName INNER JOIN users ON payments.userId = users.uid WHERE payments.invoiceNumber > 0 AND payments.planProjectId = "${data.uid}" GROUP BY payments.skill) as history ON payments.skill = history.skill INNER JOIN users ON payments.userId = users.uid WHERE payments.invoiceNumber = 0 AND payments.planProjectId = "${data.uid}" GROUP BY payments.skill`)
      
      let dateOrder = await PlanningProject.find(data.uid)
      let sisaOrder = await Database.from('projects').where('projectId', data.uid).where('isScan', 0).getCount()
      let max = await Payment.query().getMax('invoiceNumber')

      project2[0].map(item => {
        let komTotal = item.komulatifTotal ? item.komulatifTotal : item.totalSkill
        let komPrice = item.komulatifPrice ? item.komulatifPrice : item.totalPrice
        item.sisaKuantiti = item.totalOrder && item.totalOrder - komTotal
        item.sisaPrice = item.total && item.total - komPrice
        item.komulatifPrice = komPrice
        item.komulatifTotal = komTotal
        return item
      })

      project2[0].sort((a, b) => {
        var A = a["skill"]
        var B = b["skill"]
        
        if(dateOrder.skill.indexOf(A) > dateOrder.skill.indexOf(B)){
          return 1;
        }else{
          return -1;
        }
      })

      console.log('export payment', project2[0]);
      let datas = [
        project[0][0],
        project2[0],
        project3[0],
        max+1,
        dateOrder.deadline,
        sisaOrder
      ];

      await this.pdf.printPayment(datas)
      await this.excel.export(datas)

      let userPayment = await Payment.query()
      .where('planProjectId', data.uid)
      .where('invoiceNumber', 0)
      .first()

      if(userPayment){
        await Payment.query()
        .where('planProjectId', data.uid)
        .where('invoiceNumber', 0)
        .update({ invoiceNumber: max + 1, invoicePath: `invoice_${max+1}.pdf`, pathExcel: `invoice_${max+1}.xlsx` })
      }

      let filePdf = `http://cdn.tempa-bersama.co.id/invoice_${max+1}.pdf`
      let fileExcel = `http://cdn.tempa-bersama.co.id/invoice_${max+1}.xlsx`
      let resFile = {
        pdf: filePdf,
        excel: fileExcel
      }
      return response.status(200).json(await this.res.store('Export Berhasil', 200, 'SUCCESS', resFile))
    } catch (error) {
      console.log('pdf', error);
      return response.status(400).json(await this.res.store('Donwload Document Failed', 400, 'FAILED', error))
    }
  }

  async exportExcel ({ request, response, auth }) {
    try {
      let data = request.all()
      let project = await Database.raw(`SELECT payments.projectId, payments.planProjectId, planning_projects.name, planning_projects.companyName, payments.skill, ( SELECT created_at FROM payments WHERE payments.planProjectId = "${data.uid}" AND payments.invoiceNumber = 0 ORDER BY created_at ASC LIMIT 1 ) as dateFrom, SUM(price) as totalPrice FROM payments INNER JOIN master_skills on payments.skill = master_skills.skillName INNER JOIN planning_projects on payments.planProjectId = planning_projects.uid WHERE payments.planProjectId = "${data.uid}" AND payments.invoiceNumber = 0`)
      let project2 = await Database.raw(`SELECT payments.skill, ( SELECT COUNT(*) FROM projects WHERE projects.projectId = payments.planProjectId ) AS totalOrder, master_skills.price AS perUnit, ( SELECT master_skills.price * totalOrder ) AS total, COUNT(*) AS totalSkill, (CASE WHEN users.isStaff = 0 THEN master_skills.price ELSE 0 END * COUNT(*)) AS totalPrice, history.totalHistory + COUNT(*) as komulatifTotal, history.totalPriceHistory + (CASE WHEN users.isStaff = 0 THEN master_skills.price ELSE 0 END * COUNT(*)) as komulatifPrice FROM payments INNER JOIN master_skills ON payments.skill = master_skills.skillName LEFT JOIN ( SELECT payments.skill, COUNT(*) AS totalHistory, (CASE WHEN users.isStaff = 0 THEN master_skills.price ELSE 0 END * COUNT(*)) AS totalPriceHistory FROM payments INNER JOIN master_skills ON payments.skill = master_skills.skillName WHERE payments.invoiceNumber > 0 AND payments.planProjectId = "${data.uid}" GROUP BY payments.skill) as history ON payments.skill = history.skill WHERE payments.invoiceNumber = 0 AND payments.planProjectId = "${data.uid}" GROUP BY payments.skill`)
      let project3 = await Database.raw(`SELECT payments.projectId, payments.planProjectId, planning_projects.name,planning_projects.companyName, payments.skill, ( SELECT created_at FROM payments WHERE payments.planProjectId = "${data.uid}" AND payments.invoiceNumber = 0 ORDER BY created_at ASC LIMIT 1 ) as dateFrom, master_skills.price, SUM(CASE WHEN u.isStaff = 0 THEN master_skills.price ELSE 0 END) as totalPrice, user_contacts.fullName, u.nip, u.isStaff FROM payments INNER JOIN master_skills on payments.skill = master_skills.skillName INNER JOIN planning_projects on payments.planProjectId = planning_projects.uid INNER JOIN projects on payments.projectId = projects.uid LEFT JOIN users u ON payments.userId = u.uid LEFT JOIN user_contacts ON u.uid = user_contacts.userId WHERE payments.planProjectId = "${data.uid}" AND payments.invoiceNumber = 0 GROUP BY(user_contacts.fullName)`)
      
      let dateOrder = await PlanningProject.find(data.uid)
      let sisaOrder = await Database.from('projects').where('projectId', data.uid).where('isScan', 0).getCount()
      let max = await Payment.query().getMax('invoiceNumber')

      project2[0].map(item => {
        let komTotal = item.komulatifTotal ? item.komulatifTotal : item.totalSkill
        let komPrice = item.komulatifPrice ? item.komulatifPrice : item.totalPrice
        item.sisaKuantiti = item.totalOrder && item.totalOrder - komTotal
        item.sisaPrice = item.total && item.total - komPrice
        item.komulatifPrice = komPrice
        item.komulatifTotal = komTotal
        return item
      })

      let datas = [
        project[0][0],
        project2[0],
        project3[0],
        max+1,
        dateOrder.deadline,
        sisaOrder
      ];

      await this.excel.export(datas)

      let userPayment = await Payment.query()
      .where('planProjectId', data.uid)
      .where('invoiceNumber', 0)
      .first()

      if(userPayment){
        await Payment.query()
        .where('planProjectId', data.uid)
        .where('invoiceNumber', 0)
        .update({ invoiceNumber: max + 1, invoicePath: `invoice_${max+1}.pdf` })
      }

      let file = `http://cdn.tempa-bersama.co.id/invoice_${max+1}.xlsx`
      return response.status(200).json(await this.res.store('Export Berhasil', 200, 'SUCCESS', file))
    } catch (error) {
      console.log('pdf', error);
      return response.status(400).json(await this.res.store('Donwload Document Failed', 400, 'FAILED', error))
    }
  }

}

module.exports = PaymentController
