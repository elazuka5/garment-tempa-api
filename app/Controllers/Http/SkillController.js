'use strict'

// SERVICES
const ResponseService = use('App/Services/ResponseService');
const Base64Service = use('App/Services/Base64Service');
// const CheckToken = use('App//Middleware/CheckToken');
const DataHeaderService = use('App/Services/DataHeaderService');
// const CheckDbService = use('App/Services/CheckDbService');
// const ApiService = use('App/Services/ApiService');
// SERVICES

// ADONIS
const Database = use('Database')
// ADONIS

// MODELS
const MasterSkill = use('App/Models/MasterSkill')
// MODELS

class SkillController {
  constructor() {
    this.res = new ResponseService
    this.base64 = new Base64Service
    this.dataHeader = new DataHeaderService
    // this.checkJwt = new CheckToken
    // this.checkDB = new CheckDbService
    // this.fetch = new ApiService
  }

  async index ({ request, response, auth }) {
    try {
      await auth.check()
      let headerData = await this.dataHeader.getHeaderData(request)
      var { current, filter, pageSize } = headerData
        var filterParams = {
          name: filter ? (filter.type === 'name' ? filter.search : '') : '',
        }
      let skill = MasterSkill.query()
      let skillQuery = await skill
      .filter(filterParams)
      .orderBy(`created_at`, filter && filter.orderBy ? filter.orderBy : 'desc')
      .paginate(current || 1, pageSize || 10)
      return response.status(200).json(await this.res.store('Get List Success', 200, 'SUCCESS', skillQuery))
    } catch (error) {
      console.log('list skill', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }

  async storeSkill ({ request, response, auth }) {
    try {
      await auth.check()
      const data = request.all()
      const checkSkill = await MasterSkill.findBy('skillName', data.name)
      if(checkSkill){
        return response.status(200).json(await this.res.store('Nama Skill Sudah Digunakan', 406, 'FAILED'))
      }else{
        const skill = await MasterSkill.create({ 
          skillName: data.name,
          price: data.price
        })
        return response.status(200).json(await this.res.store('Berhasil Membuat Skill', 200, 'SUCCESS', skill))
      }
    } catch (error) {
      console.log('skill create', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }

  async showSkill ({ request, response, auth }) {
    try {
      await auth.check()
      const headerData = await this.dataHeader.getHeaderData(request)
      const skill = await MasterSkill.findBy('id', headerData.id)
      if(skill){
        return response.status(200).json(await this.res.store('Get Detail Success', 200, 'SUCCESS', skill))
      }else{
        return response.status(200).json(await this.res.store('Skill Tidak Ditemukan', 400, 'FAILED'))
      }
    } catch (error) {
      console.log('skill create', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }

  async updateSkill ({ request, response, auth }) {
    try {
      await auth.check()
      const data = request.all()
      const skill = await MasterSkill.find(data.id)
      if(skill){

        const skillSame = await Database.from('master_skills').where('skillName', data.name).whereNotIn('id', [data.id])
        if(skillSame.length > 0){
          return response.status(200).json(await this.res.store('Nama Skill Sudah Digunakan', 400, 'FAILED'))
        }

        // EDIT FULL PROJECT
        // let planProject = await Database.raw(`SELECT uid, JSON_SEARCH(planning_projects.skill, 'one', "${skill.skillName}") as indexSkill FROM planning_projects WHERE JSON_SEARCH(skill, 'one', "${skill.skillName}") IS NOT NULL`)
        // let resProject = planProject[0]
        // resProject.forEach(async (item) => {
        //   item.indexSkill = JSON.parse(item.indexSkill)
        //   await Database.raw(`UPDATE planning_projects SET skill = JSON_REPLACE(planning_projects.skill, "${item.indexSkill}", "${data.name}")`)
        // })
        // let planProjectId = resProject.map(item => `"${item.uid}"`).join(',')
        // console.log(444, planProject);
        // console.log(5555, planProjectId);
        // if(planProjectId){
        //   console.log(333, 'yes');
        //   await Database.raw(`UPDATE projects SET skill = REPLACE(skill, '"${skill.skillName}":', '"${data.name}":') WHERE projectId IN (${planProjectId})`)
        // }
        // EDIT FULL PROJECT

        // EDIT FULL USER
        // let user = await Database.raw(`SELECT JSON_SEARCH(skills.skill, 'one', "${skill.skillName}") as indexSkill FROM skills WHERE JSON_SEARCH(skill, 'one', "${skill.skillName}") IS NOT NULL`)
        // let resUser = user[0]
        // resUser.forEach(async (item) => {
        //   item.indexSkill = JSON.parse(item.indexSkill)
        //   await Database.raw(`UPDATE skills SET skill = JSON_REPLACE(skills.skill, "${item.indexSkill}", "${data.name}")`)
        // })
        // EDIT FULL USER

        skill.merge({ 
          // skillName: data.name,
          price: data.price
        })
        await skill.save()
        return response.status(200).json(await this.res.store('Berhasil Mengubah Skill', 200, 'SUCCESS', skill))
      }else{
        return response.status(200).json(await this.res.store('Skill Tidak Ditemukan', 400, 'FAILED'))
      }
    } catch (error) {
      console.log('skill update', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }

  async deleteSkill ({ request, response, auth }) {
    try {
      await auth.check()
      const data = request.all()
      const skill = await MasterSkill.find(data.id)
      if(skill){
        await Database.raw(`UPDATE skills SET skill = JSON_REMOVE(skill, JSON_UNQUOTE(JSON_SEARCH(skill, 'one', "${skill.skillName}"))) WHERE JSON_SEARCH(skill, 'one', "${skill.skillName}") IS NOT NULL`)
        await Database.raw(`UPDATE planning_projects SET skill = JSON_REMOVE(skill, JSON_UNQUOTE(JSON_SEARCH(skill, 'one', "${skill.skillName}"))) WHERE JSON_SEARCH(skill, 'one', "${skill.skillName}") IS NOT NULL`)
        await Database.raw(`UPDATE projects SET skill = JSON_REMOVE(skill, "$.${skill.skillName}") WHERE JSON_SEARCH(skill, 'one', "${skill.skillName}") IS NOT NULL`)
        await skill.delete()
        return response.status(200).json(await this.res.store('Berhasil Menghapus Skill', 200, 'SUCCESS', skill))
      }else{
        return response.status(200).json(await this.res.store('Skill Tidak Ditemukan', 400, 'FAILED'))
      }
    } catch (error) {
      console.log('skill Delete', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }
}

module.exports = SkillController
