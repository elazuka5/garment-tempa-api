'use strict'

// SERVICES
const ResponseService = use('App/Services/ResponseService');
// const CheckToken = use('App//Middleware/CheckToken');
const DataHeaderService = use('App/Services/DataHeaderService');
const PdfService = use("App/Services/PdfService");
// const CheckDbService = use('App/Services/CheckDbService');
// const ApiService = use('App/Services/ApiService');
// const Base64Service = use('App/Services/Base64Service');
// SERVICES

// MODELS
const User = use('App/Models/User')
const UserContact = use('App/Models/UserContact')
const Skill = use('App/Models/Skill')
const MasterSkill = use('App/Models/MasterSkill')
const Project = use("App/Models/Project");
const PlanningProject = use("App/Models/PlanningProject");
const Payment = use("App/Models/Payment");
// const Invoice = use("App/Models/Invoice");
// MODELS

// ADONIS
const Database = use('Database')
const Event = use('Event')
const Helpers = use('Helpers')
const Drive = use('Drive')
const Env = use('Env')
const fs = use('fs')
const csv = use('csv-parser');
const Redis = use('Redis')
// const userOtp = Env.get('USERKEY_OTP')
// const passOtp = Env.get('PASSKEY_OTP')
// ADONIS

class InvoiceController {
  constructor() {
    this.res = new ResponseService
    this.dataHeader = new DataHeaderService
    this.pdf = new PdfService
    // this.base64 = new Base64Service
    // this.checkDB = new CheckDbService
    // this.fetch = new ApiService
  }

  async index ({ request, view, response, auth }){
    try {
      var headerData = await this.dataHeader.getHeaderData(request)
      var { current, filter, pageSize } = headerData
      var filterParams = {
        name: filter ? (filter.type === 'name' ? filter.search : '') : '',
      }

      let userPayment = Payment.query()
      .select('payments.planProjectId', 'payments.pathExcel', 'payments.invoicePath', 'payments.updated_at', 'payments.invoiceNumber', Database.raw('sum(price) as totalPrice'))
      .with('planProject', (builder) => {
        builder.select('planning_projects.uid', 'planning_projects.name')
      })
      .where((query) => {
        if(filter && filter.dateFrom && filter.dateTo){
          // where default
          query.whereBetween('payments.updated_at', [`${filter.dateFrom} 00:00:00`, `${filter.dateTo} 23:59:59`])
        }
        return query;
      })
      .where('invoiceNumber', '>', 0)
      .innerJoin('master_skills', 'payments.skill', 'master_skills.skillName')
      .groupBy('invoiceNumber')

      var payQuery = await userPayment
      .filter(filterParams)
      .orderBy(`payments.created_at`, filter && filter.orderBy ? filter.orderBy : 'desc')
      .paginate(current || 1, pageSize || 10)

      return response.status(200).json(await this.res.store('Get List Success', 200, 'SUCCESS', payQuery))
    } catch (error) {
      console.log('list invoice', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }

  async show ({ request, view, response, auth }){
    try {
      var headerData = await this.dataHeader.getHeaderData(request)
      var payQuery = await Payment.query()
      .select('master_skills.price', 'payments.pathExcel', 'payments.skill', 'payments.invoicePath', 'payments.planProjectId', 'payments.userId', Database.raw(`COUNT(*) as totalSkill, master_skills.price * COUNT(*) as totalPrice`))
      .where('payments.invoiceNumber', headerData.invoiceNumber)
      .with('planProject', (builder) => {
        builder.select('planning_projects.uid', 'planning_projects.name')
      })
      .with('user', (builder) => {
        builder.select('users.uid', 'users.nip', 'user_contacts.fullName').leftJoin('user_contacts', 'users.uid', 'user_contacts.userId')
      })
      .innerJoin('master_skills', 'payments.skill', 'master_skills.skillName')
      .groupBy('payments.skill')
      .fetch()

      return response.status(200).json(await this.res.store('Get List Success', 200, 'SUCCESS', payQuery))
    } catch (error) {
      console.log('list', error);
      return response.status(406).json(await this.res.store('Terjadi Kesalahan', 406, 'ERROR', error))
    }
  }
}

module.exports = InvoiceController
