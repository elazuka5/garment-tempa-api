'use strict'

const ResponseService = use("App/Services/ResponseService")
const UserContact = use('App/Models/UserContact')
const Database = use('Database')

class StoreUser {
  constructor() {
    this.res = new ResponseService
  }

  // async authorize () {
  //   if (!isAdmin) {
  //     this.ctx.response.unauthorized('Not authorized')
  //     return false
  //   }

  //   return true
  // }

  
  get rules () {
    const data = this.ctx.request.all() //Add hidden field to form containing the userid
    return {
      // email: `required|email|unique:users,email,uid,${email}`,
      // phoneNumber: `required|min:5|unique:user_contacts,phoneNumber,uid,${phoneNumber}`,
      // userName: `unique:users,userName,userName,${userName}`,
      // phoneNumber: `unique:user_contacts,phoneNumber,phoneNumber,${phoneNumber}`,
      phoneNumber: `unique:users,phoneNumber,phoneNumber,${data.phoneNumber}`,
      nip: `unique:users,nip,nip,${data.nip}`
      // phoneNumber: 'required'
      // first_name: 'required',
      // last_name: 'required'
    }
    // var requestBody = this.ctx.request.all()
    // switch (requestBody.body.signupType) {
    //   case 'ace':
    //     return {
    //       email: 'required|email|unique:users,email',
    //       phoneNumber: 'required'
    //     }
    //   default:
    //     return null
    // }
  }

  // get data () {
  //   const data = this.ctx.request.all()
  //   return data
  // }

  get messages () {
    return {
      // 'email.required': 'Email required',
      // 'email.email': 'Invalid email format',
      // 'email.unique': 'Email already exist!',
      // 'phoneNumber.required': 'Phone number required',
      // 'phoneNumber.unique': 'Phone number already exist!'
    }
  }
  
  async fails (error) {
    return this.ctx.response.status(400).json(await this.res.store(error, 400, 'FAILED'))
  }
}

module.exports = StoreUser
