'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Token = use('App/Models/Token')

class CheckToken {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle (ctx, next) {
    try {
      const x = ctx.request.header('Authorization')    
      const split = x.split(' ')
      const jwtToken = split[1]

      const aa = await Token.findByOrFail('token', jwtToken)

      await next(aa)
      
    } catch (error) {
      ctx.response.header('code', 401)
        .safeHeader('shortCode', 'INVALID_TOKEN 1')
        .status(401)
        .send({
          status: {
            code: 401,
            message: 'Login required',
            shortCode: 'INVALID_TOKEN 2'
          }
        })
      return ctx.response._lazyBody.content
    }
    // call next to advance the request
    // const token = request.header('Authorization')
    // const req = token.split(' ').pop()
    // console.log(23, req);
    await next()
  }
}

module.exports = CheckToken
