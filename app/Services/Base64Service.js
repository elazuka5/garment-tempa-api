'use strict'
var moment = use('moment')
const CryptoJS = use('crypto-js')
const { Base64 } = use('js-base64')
const shortUrl = use('node-url-shortener')
const signature = 'TEMPA::INDONESIA'

class Base64Service {
  async store(data) {
    let buff = Buffer.from(data, 'base64')
    let text = buff.toString('ascii')
    return JSON.parse(text)
    // return JSON.parse(data)
  }

  async encode(data) {
    var encrypt = CryptoJS.AES.encrypt(JSON.stringify(data), signature)
    return Base64.encode(String(encrypt))
  }

  async decode(data) {
    var bytes = CryptoJS.AES.decrypt(Base64.decode(data), signature)
    var text = bytes.toString(CryptoJS.enc.Utf8)
    return JSON.parse(text)
  }

  async random(){
    const firstItem = {
      value: "0"
    };
    /*length can be increased for lists with more items.*/
    let counter = "123456789".split('')
        .reduce((acc, curValue, curIndex, arr) => {
            const curObj = {};
            curObj.value = curValue;
            curObj.prev = acc;

            return curObj;
        }, firstItem);
    firstItem.prev = counter;

    let now = Date.now();
    if (typeof performance === "object" && typeof performance.now === "function") {
        now = performance.now().toString().replace('.', '');
    }
    counter = counter.prev;
    return `${now}${Math.random().toString(16).substr(2)}${counter.value}`;
  }

  shortener(data) {
    return new Promise((resolve, reject) => {
      return shortUrl.short(data, function(err, res){
        if(!err){
          return resolve(res) 
        }else{
          return reject()
        }
      })
    })
  }
}

module.exports = Base64Service
