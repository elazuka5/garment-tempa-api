'use strict'

var xl = use('excel4node');
const Excel = use('exceljs');
const Helpers = use('Helpers');
var moment = use('moment')
// USE 'moment/locale/id'
moment.locale('id');

class ExportService {
  sum(arr, key) {
    return arr.reduce((a, b) => a + (b[key] || 0), 0);
  }

  async export(data){
  // Create a new instance of a Workbook class
  var wb = new xl.Workbook();
  
  // Add Worksheets to the workbook
  var ws = wb.addWorksheet('Rincian Upah');
  var ws2 = wb.addWorksheet('Laporan Upah');
  var ws3 = wb.addWorksheet('TDD Upah Mingguan');
  
  // Create a reusable style
  var styleHeader = wb.createStyle({
    font: {
      size: 14,
      bold: true
    },
    numberFormat: '$#,##0.00; ($#,##0.00); -',
  });

  var styleTable = wb.createStyle(
    {font: {size: 14, bold: true}, alignment: { horizontal: 'center', vertical: 'center', wrapText: true }, border: { top: { style:'thin' }, bottom: { style:'thin' }, left: { style:'thin' }, right: { style:'thin' } }}
  )

  var styleTable2 = wb.createStyle(
    {font: {size: 12, bold: true}, alignment: { horizontal: 'center', vertical: 'center', wrapText: true }, border: { top: { style:'thin' }, bottom: { style:'thin' }, left: { style:'thin' }, right: { style:'thin' } }}
  )

  var styleDataTable2 = wb.createStyle(
    {border: { top: { style:'thin' }, bottom: { style:'thin' }, left: { style:'thin' }, right: { style:'thin' } }}
  )

  // ======================== SHEET 1 =======================================
  ws.cell(2, 2, 2, 4, true)
    .string(data[0].companyName)
    .style(styleHeader)

  ws.cell(3, 2, 3, 5, true)
    .string('LAPORAN REKAP UPAH MINGGUAN')
    .style(styleHeader)
  
  ws.cell(4, 2, 4, 5, true)
    .string(`PERIODE : ${moment(data[0].dateFrom).format('DD-MMM-YYYY')} s/d ${moment().format('DD-MMM-YYYY')}`)
    .style(styleHeader)

  ws.cell(6, 2).string('No.')
  .style(styleTable)

  ws.cell(6, 3, 6, 13, true).string('NAMA JOB')
  .style(styleTable)

  ws.cell(6, 14, 6, 15, true).string('UPAH')
  .style(styleTable)

  ws.cell(7, 2).number(1)
  .style(styleTable)

  ws.cell(7, 3, 7, 13, true).string(data[0].name)
  .style(styleTable)

  ws.cell(7, 14, 7, 15, true).string(`Rp ${data[0].totalPrice ? data[0].totalPrice.toLocaleString() : '0'}`)
  .style(styleTable)

  ws.cell(8, 2, 8, 4, true).string(`Di Cetak Tanggal : ${moment().format('DD-MMM-YYYY')}`)
  .style({font: {size: 12}})

  ws.cell(8, 13).string('Total :')
  .style(styleHeader)

  ws.cell(8, 14, 8, 15, true).string(`Rp ${data[0].totalPrice ? data[0].totalPrice.toLocaleString() : '0'}`)
  .style({font: {size: 14, bold: true}, alignment: { horizontal: 'center' }})

  ws.cell(11, 4, 11, 6, true).string('DIKETAHUI / DISETUJUI')
  .style(styleHeader)
  .style({alignment: { horizontal: 'center', vertical: 'center', wrapText: true }})

  ws.cell(18, 4, 18, 6, true).string('( ..... )')
  .style(styleHeader)
  .style({alignment: { horizontal: 'center', vertical: 'center', wrapText: true }})

  ws.cell(11, 10, 11, 12, true).string('DIBUAT')
  .style(styleHeader)
  .style({alignment: { horizontal: 'center', vertical: 'center', wrapText: true }})

  ws.cell(18, 10, 18, 12, true).string('( ..... )')
  .style(styleHeader)
  .style({alignment: { horizontal: 'center', vertical: 'center', wrapText: true }})
  // ======================== SHEET 1 =======================================


  // ======================== SHEET 2 =======================================
  ws2.cell(2, 2, 2, 4, true)
    .string(data[0].companyName)
    .style(styleHeader)
  
  ws2.cell(4, 2, 4, 5, true)
    .string(`Tanggal Order : ${moment(data[4]).format('DD-MMM-YYYY')}`)
    .style(styleHeader)
  
  ws2.cell(5, 2, 5, 3, true)
    .string(`No. Order :`)
    .style(styleHeader)

  ws2.cell(5, 4, 5, 7, true)
  .string('')

  ws2.cell(4, 12, 4, 15, true)
    .string(`NAMA JOB : ${data[0].name}`)
    .style(styleHeader)

  ws2.cell(7, 2, 9, 2, true).string('No.')
  .style(styleTable2)

  ws2.cell(7, 3, 9, 6, true).string('PROSES')
  .style(styleTable2)

  ws2.cell(7, 7, 9, 7, true).string('KUANTITI ORDER')
  .style(styleTable2)

  ws2.column(8).setWidth(20);
  ws2.cell(7, 8, 9, 8, true).string('HARGA SATUAN')
  .style(styleTable2)

  ws2.column(9).setWidth(20);
  ws2.cell(7, 9, 9, 9, true).string('PLAFON UPAH')
  .style(styleTable2)

  ws2.column(10).setWidth(15);
  ws2.column(11).setWidth(15);

  ws2.cell(7, 10, 7, 11, true).string('UPAH PERMINGGU')
  .style(styleTable2)

  ws2.cell(8, 10, 8, 11, true).string(`${moment(data[0].dateFrom).format('DD-MMM-YYYY')} s/d ${moment().format('DD-MMM-YYYY')}`)
  .style(styleTable2)

  ws2.cell(9, 10).string('JUMLAH')
  .style(styleTable2)

  ws2.cell(9, 11).string('UPAH')
  .style(styleTable2)

  ws2.column(12).setWidth(15);
  ws2.column(13).setWidth(15);
  ws2.cell(7, 12, 8, 13, true).string('KOMULATIF')
  .style(styleTable2)
  
  ws2.cell(9, 12).string('JUMLAH')
  .style(styleTable2)

  ws2.cell(9, 13).string('UPAH')
  .style(styleTable2)

  ws2.cell(7, 14, 9, 14, true).string('SISA KUANTITI')
  .style(styleTable2)

  ws2.column(15).setWidth(20);
  ws2.cell(7, 15, 9, 15, true).string('SISA PLAFON')
  .style(styleTable2)

  data[1].forEach((item, i) => {
    ws2.cell(i + 10, 2).number(i + 1).style(styleDataTable2).style({alignment: { horizontal: 'center', vertical: 'center', wrapText: true }});
    ws2.cell(i + 10, 3, i + 10, 6, true).string(item.skill).style(styleDataTable2);
    ws2.cell(i + 10, 7).number(item.totalOrder).style(styleDataTable2);
    ws2.cell(i + 10, 8).string(`Rp ${item.perUnit.toLocaleString()}`).style(styleDataTable2);
    ws2.cell(i + 10, 9).string(`Rp ${item.total.toLocaleString()}`).style(styleDataTable2);
    ws2.cell(i + 10, 10).number(item.totalSkill).style(styleDataTable2);
    ws2.cell(i + 10, 11).string(`Rp ${item.totalPrice.toLocaleString()}`).style(styleDataTable2);
    ws2.cell(i + 10, 12).number(item.komulatifTotal).style(styleDataTable2);
    ws2.cell(i + 10, 13).string(`Rp ${item.komulatifPrice.toLocaleString()}`).style(styleDataTable2);
    ws2.cell(i + 10, 14).number(item.sisaKuantiti).style(styleDataTable2);
    ws2.cell(i + 10, 15).string(`Rp ${item.sisaPrice.toLocaleString()}`).style(styleDataTable2);
  })

  var lastRow = data[1].length + 10
  ws2.cell(lastRow, 2, lastRow, 6, true).string('TOTAL')
  .style(styleTable2)

  ws2.cell(lastRow, 7).string('')
  .style(styleTable2)

  ws2.cell(lastRow, 8).string(`Rp ${this.sum(data[1], 'perUnit').toLocaleString()}`)
  .style(styleTable2)

  ws2.cell(lastRow, 9).string(`Rp ${this.sum(data[1], 'total').toLocaleString()}`)
  .style(styleTable2)

  ws2.cell(lastRow, 10).string('')
  .style(styleTable2)

  ws2.cell(lastRow, 11).string(`Rp ${this.sum(data[1], 'totalPrice').toLocaleString()}`)
  .style(styleTable2)

  ws2.cell(lastRow, 12).string('')
  .style(styleTable2)

  ws2.cell(lastRow, 13).string(`Rp ${this.sum(data[1], 'komulatifPrice').toLocaleString()}`)
  .style(styleTable2)

  ws2.cell(lastRow, 14).string('')
  .style(styleTable2)

  ws2.cell(lastRow, 15).string(`Rp ${this.sum(data[1], 'sisaPrice').toLocaleString()}`)
  .style(styleTable2)

  ws2.cell(lastRow + 3, 4, lastRow + 3, 6, true).string('DIKETAHUI / DISETUJUI')
  .style(styleHeader)
  .style({alignment: { horizontal: 'center', vertical: 'center', wrapText: true }})

  ws2.cell(lastRow + 10, 4, lastRow + 10, 6, true).string('( ..... )')
  .style(styleHeader)
  .style({alignment: { horizontal: 'center', vertical: 'center', wrapText: true }})

  ws2.cell(lastRow + 3, 10, lastRow + 3, 12, true).string('DIBUAT')
  .style(styleHeader)
  .style({alignment: { horizontal: 'center', vertical: 'center', wrapText: true }})

  ws2.cell(lastRow + 10, 10, lastRow + 10, 12, true).string('( ..... )')
  .style(styleHeader)
  .style({alignment: { horizontal: 'center', vertical: 'center', wrapText: true }})
  // ======================== SHEET 2 =======================================


  // ======================== SHEET 3 =======================================
  ws3.cell(2, 2, 2, 4, true)
    .string(data[0].companyName)
    .style(styleHeader)

  ws3.cell(3, 2, 3, 5, true)
    .string('LAPORAN UPAH MINGGUAN')
    .style(styleHeader)

  ws3.cell(4, 2, 4, 6, true)
    .string(`NAMA JOB : ${data[0].name}`)
    .style(styleHeader)
  
  ws3.cell(5, 2, 5, 5, true)
    .string(`PERIODE : ${moment(data[0].dateFrom).format('DD-MMM-YYYY')} s/d ${moment().format('DD-MMM-YYYY')}`)
    .style(styleHeader)

  ws3.cell(7, 2, 7, 2, true).string('No.')
  .style(styleTable2)

  ws3.cell(7, 3, 7, 5, true).string('NO. PEGAWAI')
  .style(styleTable2)

  ws3.cell(7, 6, 7, 11, true).string('NAMA PEGAWAI')
  .style(styleTable2)

  ws3.column(12).setWidth(20);
  ws3.cell(7, 12).string('UPAH')
  .style(styleTable2)

  ws3.column(13).setWidth(20);
  ws3.cell(7, 13).string('PARAF')
  .style(styleTable2)

  data[2].forEach((item, i) => {
    ws3.cell(i + 8, 2).number(i + 1).style(styleDataTable2).style({alignment: { horizontal: 'center', vertical: 'center', wrapText: true }});
    ws3.cell(i + 8, 3, i + 8, 5, true).string(`${item.nip}`).style(styleDataTable2);
    ws3.cell(i + 8, 6, i + 8, 11, true).string(`${item.fullName} (${item.isStaff ? 'Staff' : 'Non Staff'})`).style(styleDataTable2);
    ws3.cell(i + 8, 12).string(`Rp ${item.totalPrice.toLocaleString()}`).style(styleDataTable2);
    ws3.cell(i + 8, 13).string('').style(styleDataTable2);
  })

  var lastRow2 = data[2].length + 8
  ws3.cell(lastRow2, 2, lastRow2, 5, true).string('TOTAL')
  .style(styleTable2)

  ws3.cell(lastRow2, 6, lastRow2, 11, true).number(data[2].length)
  .style(styleTable2)

  ws3.cell(lastRow2, 12).string(`Rp ${data[0].totalPrice ? data[0].totalPrice.toLocaleString() : '0'}`)
  .style(styleTable2)

  ws3.cell(lastRow2, 13).string('')
  .style(styleTable2)

  ws3.cell(lastRow2 + 3, 4, lastRow2 + 3, 6, true).string('DIKETAHUI / DISETUJUI')
  .style(styleHeader)
  .style({alignment: { horizontal: 'center', vertical: 'center', wrapText: true }})

  ws3.cell(lastRow2 + 10, 4, lastRow2 + 10, 6, true).string('( ..... )')
  .style(styleHeader)
  .style({alignment: { horizontal: 'center', vertical: 'center', wrapText: true }})

  ws3.cell(lastRow2 + 3, 10, lastRow2 + 3, 12, true).string('DIBUAT')
  .style(styleHeader)
  .style({alignment: { horizontal: 'center', vertical: 'center', wrapText: true }})

  ws3.cell(lastRow2 + 10, 10, lastRow2 + 10, 12, true).string('( ..... )')
  .style(styleHeader)
  .style({alignment: { horizontal: 'center', vertical: 'center', wrapText: true }})

  const tmp = Helpers.tmpPath()  
  wb.write(tmp + '/' + 'invoice_' + data[3] + '.xlsx');
  }

}

module.exports = ExportService