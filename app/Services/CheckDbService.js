'use strict'

var jwt = use('jsonwebtoken');
const User = use('App/Models/User')

// var moment = use('moment')
// const CryptoJS = use('crypto-js')
const { Base64 } = use('js-base64')
// const shortUrl = use('node-url-shortener')
// const signature = 'MENTALKU::2020'

class CheckDbService {
  async checkEmail(data){
    try {
      const emailCheck = await User.findBy('email', data)
      if(emailCheck){
        return emailCheck.email
      }
    } catch (error) {
      return error
    }
  }
}

module.exports = CheckDbService
