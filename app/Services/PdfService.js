'use strict'

const Base64Service = use("App/Services/Base64Service");

const fs = use("fs");
const PDFDocument = use("pdfkit");
const { Base64Encode } = use('base64-stream');
const Helpers = use('Helpers')
const Drive = use('Drive')
const QRCode = use('qrcode')
var moment = use('moment')
// USE 'moment/locale/id'
moment.locale('id');

var fonts = {
	Roboto: {
    // normal: 'fonts/Roboto-Regular.ttf',
    normal: Helpers.publicPath('/assets/fonts/Roboto-Regular.ttf'),
    bold: Helpers.publicPath('/assets/fonts/Roboto-Medium.ttf'),
    italics: Helpers.publicPath('/assets/fonts/Roboto-Italic.ttf'),
    bolditalics: Helpers.publicPath('/assets/fonts/Roboto-MediumItalic.ttf'),
		// bold: 'fonts/Roboto-Medium.ttf',
		// italics: 'fonts/Roboto-Italic.ttf',
		// bolditalics: 'fonts/Roboto-MediumItalic.ttf'
	}
};
var PdfPrinter = use('pdfmake');
var printer = new PdfPrinter(fonts);

class PdfService {
  constructor() {
    this.base64 = new Base64Service
  }

  sum(arr, key) {
    return arr.reduce((a, b) => a + (b[key] || 0), 0);
  }

  async createInvoice(invoice) {
    return new Promise((resolve) => {
      let doc = new PDFDocument({ size: "A4", margin: 50 });
    
      this.generateHeader(doc);
      this.generateCustomerInformation(doc, invoice);
      // this.generateInvoiceTable(doc, invoice);
      this.generateFooter(doc);
  
      // var finalString = ''; // contains the base64 string
      // var stream = doc.pipe(new Base64Encode());  

      doc.end();
      var stream = doc.pipe(new Base64Encode());
      var finalString = ''; 
      stream.on('data', function(chunk) {
        finalString += chunk;
      });
      stream.on('end', function() {
        return resolve(finalString)
      });
    })
  }

  async printPayment(data){
    return new Promise((resolve, reject) => {
      var data3 = data[2].map((res, i) => {
        return [
          {text: i+1, alignment: "center"},
          {text: res.nip, alignment: "center"},
          {text: `${res.fullName} (${res.isStaff ? 'Staff' : 'Non Staff'})`, alignment: "left"},
          {text: `Rp ${res.totalPrice}`, alignment: "left"},
          {text: "", alignment: "left"}
        ]
      });
      console.log('data export', data[1]);
      var data2 = data[1].map((res, i) => {
        return [
          {text: i+1, alignment: "center"},
          {text: res.skill, alignment: "left"},
          {text: res.totalOrder, alignment: "left"},
          {text: `Rp ${res.perUnit}`, alignment: "left"},
          {text: `Rp ${res.total}`, alignment: "left"},
          {text: res.totalSkill, alignment: "left"},
          {text: `Rp ${res.totalPrice}`, alignment: "left"},
          {text: res.komulatifTotal, alignment: "left"},
          {text: `Rp ${res.komulatifPrice}`, alignment: "left"},
          {text: res.sisaKuantiti, alignment: "left"},
          {text: `Rp ${res.sisaPrice}`, alignment: "left"}
        ]
      });
      var docDefinition = {
        pageOrientation: 'landscape',
        pageMargins: [ 10, 10, 10, 50 ],
        content: [
          { text: "RINCIAN UPAH", style: "header", alignment: "center" },
          { text: data[0].companyName, style: "subheader" },
          { text: "LAPORAN REKAP UPAH MINGGUAN", style: "subheader" },
          { text: `PERIODE : ${moment(data[0].dateFrom).format('DD-MMM-YYYY')} s/d ${moment().format('DD-MMM-YYYY')}`, bold: true, fontSize: 12, margin: [0, 10, 0, 5] },
          {
            table: {
              widths: [30, "*", 100],
              margin: [0, 10, 0, 0],
              body: [
                [
                  { text: "No.", style: "tableHeader", alignment: "center" },
                  { text: "NAMA JOB", style: "tableHeader", alignment: "center" },
                  { text: "UPAH", style: "tableHeader", alignment: "center" },
                ],
                [
                  { text: "1", alignment: "center" },
                  { text: data[0].name, alignment: "center" },
                  { text: `Rp ${data[0].totalPrice ? data[0].totalPrice.toLocaleString() : 0}`, alignment: "center" },
                ],
              ],
            },
          },
          {
            columns: [
              {
                text: `Di Cetak Tanggal : ${moment().format('DD-MMM-YYYY')}`,
                alignment: "left",
                // 	width: 600
              },
              {
                text: `Total : Rp ${data[0].totalPrice ? data[0].totalPrice.toLocaleString() : 0}`,
                bold: true,
                alignment: "right",
              },
            ],
            margin: [0, 10, 0, 0],
          },
          // {
          //   absolutePosition: { x: 12, y: 400 },
          //   alignment: "center",
          //   columns: [
          //     {
          //       text: "DIKETAHUI / DISETUJUI",
          //     },
          //     {
          //       text: "DIBUAT",
          //     },
          //   ],
          //   margin: [0, 0, 0, 100],
          // },
          // {
          //   absolutePosition: { x: 12, y: 500 },
          //   alignment: "center",
          //   columns: [
          //     {
          //       text: "( ................................ )",
          //     },
          //     {
          //       text: "( ................................ )",
          //     },
          //   ],
          // },


          { text: "LAPORAN UPAH", pageBreak: 'before', style: "header", alignment: "center" },
          {
            columns: [
              {
                text: `Tanggal Order : ${moment(data[4]).format('DD-MMM-YYYY')}`,
                alignment: "left",
                width: "*",
                bold: true,
              },
              {
                text: `NAMA JOB: ${data[0].name}`,
                alignment: "right",
                bold: true,
              },
            ],
            margin: [0, 10, 0, 10],
          },
          { text: "No. Order : ", alignment: "left", width: "*", bold: true, margin: [0, 0, 0, 5] },
          {
            table: {
              widths: [30, 150, "*", "*", "*", "*", "*", "*", "*", "*", "*"],
              // widths: '*',
              // margin: [0, 10, 0, 0],
              body: [
                  [
                    { text: "No.", rowSpan: 3, style: "tableHeader2" },
                    { text: "PROSES", rowSpan: 3, style: "tableHeader2" },
                    { text: "KUANTITI ORDER", rowSpan: 3, style: "tableHeader2" },
                    { text: "HARGA SATUAN", rowSpan: 3, style: "tableHeader2" },
                    { text: "PLAFON UPAH", rowSpan: 3, style: "tableHeader2" },
                    { text: "UPAH PERMINGGU", colSpan: 2, style: "tableHeader2" },
                    "",
                    { text: "KOMULATIF", colSpan: 2, rowSpan: 2, style: "tableHeader2" },
                    "",
                    { text: "SISA KUANTITI", rowSpan: 3, style: "tableHeader2" },
                    { text: "SISA PLAFON", rowSpan: 3, style: "tableHeader2" },
                  ],
                  ["", "", "", "", "", { text: `${moment(data[0].dateFrom).format('DD-MMM-YYYY')} s/d ${moment().format('DD-MMM-YYYY')}`, colSpan: 2, style: "tableHeader2" }, "", "", "", { text: "UPAH", style: "tableHeader2" }, ""],
                  ["", "", "", "", "", { text: "JUMLAH", style: "tableHeader2" }, { text: "UPAH", style: "tableHeader2" }, { text: "JUMLAH", style: "tableHeader2" }, { text: "UPAH", style: "tableHeader2" }, "", ""],
                  ...data2,
                  [
                    { text: "TOTAL", colSpan: 2, style: "tableHeader2" },
                    "",
                    "",
                    { text: `Rp ${this.sum(data[1], 'perUnit')}`},
                    { text: `Rp ${this.sum(data[1], 'total')}`},
                    "",
                    { text: `Rp ${this.sum(data[1], 'totalPrice')}`},
                    "",
                    { text: `Rp ${this.sum(data[1], 'komulatifPrice')}`},
                    "",
                    { text: `Rp ${this.sum(data[1], 'sisaPrice')}`},
                  ],
              ]              
            }
          },
          {
            absolutePosition: { x: 12, y: 435 },
            alignment: "center",
            columns: [
              {
                text: "DIKETAHUI / DISETUJUI",
              },
              {
                text: "DIBUAT",
              },
            ],
            margin: [0, 0, 0, 100],
          },
          {
            absolutePosition: { x: 12, y: 530 },
            alignment: "center",
            columns: [
              {
                text: "( ................................ )",
              },
              {
                text: "( ................................ )",
              },
            ],
          },
          
          
          { text: "TTD UPAH MINGGUAN", pageBreak: 'before', style: "header", alignment: "center" },
          { text: data[0].companyName, style: "subheader" },
          { text: "LAPORAN UPAH MINGGUAN", style: "subheader" },
          { text: `NAMA JOB: ${data[0].name}`, style: "subheader" },
          { text: `PERIODE : ${moment(data[0].dateFrom).format('DD-MMM-YYYY')} s/d ${moment().format('DD-MMM-YYYY')}`, bold: true, fontSize: 12, margin: [0, 10, 0, 5] },
          {
            table: {
              widths: [30, 150, "*", 100, 100],
              margin: [0, 10, 0, 0],
              body: [
                [
                  { text: "No.", style: "tableHeader", alignment: "center" },
                  { text: "NO. PEGAWAI", style: "tableHeader", alignment: "center" },
                  { text: "NAMA PEGAWAI", style: "tableHeader", alignment: "left" },
                  { text: "UPAH", style: "tableHeader", alignment: "left" },
                  { text: "PARAF", style: "tableHeader", alignment: "left" },
                ],
                
                ...data3,
        
                [{ colSpan: 2, text: "TOTAL PEGAWAI:", alignment: "left" }, {}, { text: data3.length, alignment: "left" }, { colSpan: 2, text: `Rp ${data[0].totalPrice ? data[0].totalPrice.toLocaleString() : 0}`, alignment: "left" }, {}],
              ],
            },
          },
          {
            columns: [
              {
                text: `Di Cetak Tanggal : ${moment().format('DD-MMM-YYYY')}`,
                alignment: "left",
              },
              {
                text: `Total : Rp ${data[0].totalPrice ? data[0].totalPrice.toLocaleString() : 0}`,
                bold: true,
                alignment: "right",
              },
            ],
            margin: [0, 10, 0, 0],
          },
          {
            absolutePosition: { x: 12, y: 400 },
            alignment: "center",
            columns: [
              {
                text: "DIKETAHUI / DISETUJUI",
              },
              {
                text: "DIBUAT",
              },
            ],
            margin: [0, 0, 0, 100],
          },
          {
            absolutePosition: { x: 12, y: 500 },
            alignment: "center",
            columns: [
              {
                text: "( ................................ )",
              },
              {
                text: "( ................................ )",
              },
            ],
          },

        ],        
        styles: {
          header: {
            fontSize: 18,
            bold: true,
            margin: [0, 0, 0, 10]
          },
          subheader: {
            fontSize: 14,
            bold: true,
            margin: [0, 5, 0, 0]
          },
          tableExample: {
            margin: [0, 5, 0, 15]
          },
          tableHeader: {
            bold: true,
            fontSize: 13,
            color: 'black'
          },
          tableHeader2: {
            bold: true,
            fontSize: 10,
            color: 'black',
            alignment: "center"
          },
          fontStyle: {
            fontSize: 10
          }
        },
        defaultStyle: {
          // fontSize: 7,
          // bold: true
          // alignment: 'justify'
        }
      }

      var doc = printer.createPdfKitDocument(docDefinition);
      const tmp = Helpers.tmpPath()
      doc.pipe(fs.createWriteStream(tmp + '/' + 'invoice_' + data[3] + '.pdf'));
      doc.end();
      return resolve('Success')
    })
  }

  async printQrv2(data, uidProject, fileName){
    return new Promise(async (resolve, reject) => {
      var docDefinition = {
        pageOrientation: 'landscape',
        pageMargins: [ 10, 10 ],
        content: [],
        styles: {
          header: {
            fontSize: 18,
            bold: true,
            margin: [0, 0, 0, 10]
          },
          subheader: {
            fontSize: 16,
            bold: true,
            margin: [0, 10, 0, 5]
          },
          tableExample: {
            margin: [0, 5, 0, 15]
          },
          tableHeader: {
            bold: true,
            fontSize: 13,
            color: 'black'
          },
          fontStyle: {
            fontSize: 10
          }
        },
        defaultStyle: {
          fontSize: 7,
          bold: true
          // alignment: 'justify'
        }
      }

      for (let res of data) {
        if(res){
          let headerInfo = res.header.length > 0 && res.header.map(item => {
            return {text: item, style: { fontSize: 16, bold: true }}
          });
          let valueInfo = res.value.length > 0 && res.value.map(item => {
            return {text: item, style: { fontSize: 16, bold: true }}
          });
          let encodeUser = await this.base64.encode(res.uid)
          let val = {
            unbreakable: true,
            stack: [
              {
                style: "tableExample",
                table: {
                  widths: ["auto", "*"],
                  body: [
                    [
                      { qr: `${encodeUser}`, fit: "100", noWrap: true },
                      [
                        { text: `${res.projectName}`, alignment: "center", style: "fontStyle" },
                        // { text: `${res.description && res.description.NAMA ? res.description.NAMA : '-'}`, alignment: "left", style: "fontStyle" },
                        {
                          columns: [
                            {
                              text: `${res.description && res.description.NAMA ? res.description.NAMA : '-'}`,
                              alignment: "left",
                              style: "fontStyle",
                              width: 150
                            },
                            {
                              text: '',
                              width: 200
                            },
                            {
                              text: `${res.description && res.description['NO POT'] ? res.description['NO POT'] : '-'} / ${res.description && res.description['NO SERI'] ? res.description['NO SERI'] : '-'}`,
                              // alignment: "right",
                              style: "fontStyle"
                            }
                          ],
                          // margin: [0, 10, 0, 10],
                        },
                        {
                          columns: [
                            {
                              width: 150,
                              style: "fontStyle",
                              text: [`${res.description && res.description.PANGKAT ? res.description.PANGKAT : '-'}\n`, `${res.description && res.description.NIP ? res.description.NIP : '-'}`],
                            },
                            {
                              width: 300,
                              style: "fontStyle",
                              text: [`${res.description && res.description.KESATUAN ? res.description.KESATUAN : '-'}`, `${res.description && res.description['KET.'] ? res.description['KET.'] : '-'}`],
                            },
                            {
                              width: "*",
                              alignment: "right",
                              table: {
                                style: "fontStyle",
                                body: [
                                  [
                                    { text: "Mdl", style: "fontStyle" },
                                    { text: "Uk", style: "fontStyle" },
                                    { text: "Krp", style: "fontStyle" },
                                    { text: "Aks", style: "fontStyle" },
                                    { text: "Qc", style: "fontStyle" },
                                  ],
                                  ["\n", "\n", "\n", "\n", "\n"],
                                ],
                              },
                            },
                          ],
                        },
                        "\n",
                        {
                          table: {
                            widths: '*',
                            body: [ headerInfo, valueInfo ]
                          },
                        },
                      ],
                    ],
                  ],
                },
              },
            ]          
          }
          docDefinition.content.push(val)
        }
      }

      var doc = printer.createPdfKitDocument(docDefinition);
      const tmp = Helpers.tmpPath()
      doc.pipe(fs.createWriteStream(tmp + '/' + fileName.split(' ').join('-') + '.pdf'));
      doc.end();
      return resolve('Success')
    })
  }

  async createPrintQr(data) {
    return new Promise(async (resolve) => {
      // var doc = new PDFDocument({ size: "A4", margin: 50, autoFirstPage: false});
    
      // this.generateHeader(doc);
      // this.generateCustomerInformation(doc, data);
      // // this.generateInvoiceTable(doc, data);
      // this.generateFooter(doc);
  
      // // var finalString = ''; // contains the base64 string
      // // var stream = doc.pipe(new Base64Encode());  
    
      var doc = new PDFDocument;
      // doc.fontSize(25);
      // doc.text('Portfolio');
      doc
      .fontSize(20)
      doc
      .text(
        `QR-CODE PROJECT ${data.name}`,
        { align: "center", width: 500 }
      );
      // let base64Qr = await QRCode.toDataURL(JSON.stringify('TMP::PROJECT::fe0da744-8c92-47c2-ba89-8f6668a91ba5'))
      // console.log(2222, base64Qr);
    //   data.forEach(async (project) => {
    //     var project_customer = project;
    //     // project_customer.forEach((item, index) => {
    //     //   // const code = Helpers.publicPath('/assets/img/code.png')
    //     //   doc.image(`${code}`, index + 50, index + 45, {width: 200})
          
    //     // })
    //     let base64Qr = await QRCode.toString(JSON.stringify(item), {type:'svg'})
    //     // console.log(1234, base64Qr);
    //     doc.addPage();
    //     // doc.text(`${project_customer}`);
    //   });

    //   // doc.addPage();
    //   // doc.text('project_customer');

    //   doc.end();
    
    //   const buffers = []
    //   doc.on("data", buffers.push.bind(buffers))
    //   doc.on("end", () => {
    //     const pdfData = Buffer.concat(buffers)
    //     resolve(pdfData.toString("base64"))
    //   })

    // let base64Qr = await QRCode.toDataURL(JSON.stringify(Math.random()))
    // var buffer = new Buffer.from(base64Qr.split(',')[1] || '', 'base64');
    // const code = Helpers.publicPath('/assets/img/code.png')
    // console.log(222, buffer);


    // soaltest.forEach((_item, _index) => {
    //   var startQuizX = 60;
    //   var startQuizY = 630;
    //   var sisaQuizY = 20;
    //   var spage = pagess[_index+1];
    //   if(_item.length > 30){
    //     _item.pop()
    //   }
    //   _item.forEach((res, key) => {
    //     return spage.drawText(`${res}`, {
    //       x: startQuizX,
    //       y: startQuizY - (key * sisaQuizY) - _index * 2,
    //       size: res && res.length > 70 ? 8 : 10,
    //       color: rgb(0, 0, 0),
    //     });
    //   });
    // });

for(let item of data.project) {
  doc.addPage();
  var i = 0;
  for (const res of item) {
    if(res){
      let n = i++;
      // n %= 5;
      let idProject = {
        "uid": res[0]
      }
      let encodeUser = await this.base64.encode(idProject)
      let base64Qr = await QRCode.toDataURL(JSON.stringify(encodeUser))
      var buffer = new Buffer.from(base64Qr.split(',')[1] || '', 'base64');
      var left = n % 2 === 0 ? n * 0 : 200;
      var tops = n >= 2 ? n % 2 ? (n-1) * 50 : n * 50 : 0;
      // var left = n * 100;
      // var tops = n === 0 ? 0 : 100;
      doc
      .image(buffer, left, tops, {height: 100})
      // .moveDown()
      // .fontSize(7)
      // .text(`nama: ${res[1]}`)

      doc
      .fontSize(7)
      .text(`nama: ${res[1]}`, (left + 7), tops)
    }
  }
}



      // invoice.map(async (project) => {
      //   doc.addPage();
      //   project.map(async (item, index) => {
      //     // if(item){
      //       // var rtt = Promise.all(this.qrqq((res) => res))

      //       // doc.image(rtt, 10, 10, {height: 200});
            
      //       // let base64Qr = await QRCode.toDataURL(JSON.stringify('dd'))
      //       // var buffer = new Buffer.from(base64Qr.split(',')[1] || '', 'base64');

      //       let base64Qr = await QRCode.toDataURL(JSON.stringify(Math.random()))
      //       var buffer = new Buffer.from(base64Qr.split(',')[1] || '', 'base64');
      //       await doc.image(buffer, (50 * index), 10, {height: 50});  
      //       // console.log(12, 'dads')
      //       // return this.qrqq(doc, buffer, () => console.log('oke'))
            
            
      //       // return SVGtoPDF(doc, base64Qr, 0, 0)
      //     // }
      //   })
        
      // })
      // console.log(11, qq);

      const publicPath = Helpers.tmpPath()
      doc.pipe(fs.createWriteStream(publicPath + '/' + data.uid + '.pdf'));
      doc.end();
      // await Drive.put(`${tmpPath}`, Buffer.from('Hello world!'))
      // await Drive.move('code.pdf', `${tmpPath}/code.pdf`)
      // let isExist = await Drive.exists(tmpPath);
      // if(isExist){
      //   await Drive.delete(tmpPath)
      // }
      return resolve('Print Success')
      // var stream = doc.pipe(new Base64Encode());
      // var finalString = ''; 
      // stream.on('data', function(chunk) {
      //   finalString += chunk;
      // });

      // stream.on('end', function() {
      //   return resolve(finalString)
      // });
    
    })

    

    // stream.on('data', function(chunk) {
    //   finalString += chunk;
    // });
    
    // stream.on('end', () => finalString )
    // console.log(123, stream);

    // doc.pipe(fs.createWriteStream(path));
  }
  
  generateHeader(doc) {
    const logo = Helpers.publicPath('/assets/img/logo_tempa_bersama-biru.png')
    doc
      .image(`${logo}`, 50, 45, {width: 200})
      .fillColor("#444444")
      .fontSize(20)
      // .text("Tempa Bersama", 110, 57)
      .fontSize(10)
      // .text("Tempa Bersama", 200, 50, { align: "right" })
      .text("Jl. Kebayoran Lama 5/1C, RT.8/RW.1,", 200, 50, { align: "right" })
      .text("North Sukabumi, Kebonjeruk, West Jakarta City,", 200, 65, { align: "right" })
      .text("Jakarta 11540", 200, 80, { align: "right" })
      .moveDown();
  }
  
  generateCustomerInformation(doc, invoice) {
    doc
      .fillColor("#444444")
      .fontSize(20)
      .text("Invoice", 50, 160);
  
    this.generateHr(doc, 185);
  
    const customerInformationTop = 200;
  
    doc
      .fontSize(10)
      .text("Invoice Number:", 50, customerInformationTop)
      .font("Helvetica-Bold")
      // .text(invoice.invoice_nr, 150, customerInformationTop)
      .font("Helvetica")
      .text("Invoice Date:", 50, customerInformationTop + 15)
      .text(this.formatDate(new Date()), 150, customerInformationTop + 15)
      // .text("Balance Due:", 50, customerInformationTop + 30)
      // .text(
      //   this.formatCurrency(invoice.subtotal - invoice.paid),
      //   150,
      //   customerInformationTop + 30
      // )
  
      .font("Helvetica-Bold")
      .text(invoice.user.fullName, 300, customerInformationTop)
      .font("Helvetica")
      .text(invoice.user.nip, 300, customerInformationTop + 15)
      .text(invoice.user.phoneNumber, 300, customerInformationTop + 30)
      // .text(
      //   invoice.user.city +
      //     ", " +
      //     invoice.user.state +
      //     ", " +
      //     invoice.user.country,
      //   300,
      //   customerInformationTop + 30
      // )
      .moveDown();
  
    this.generateHr(doc, 252);
  }
  
  generateInvoiceTable(doc, invoice) {
    let i;
    const invoiceTableTop = 330;
  
    doc.font("Helvetica-Bold");
    this.generateTableRow(
      doc,
      invoiceTableTop,
      "Item",
      "Description",
      "Unit Cost",
      "Quantity",
      "Line Total"
    );
    this.generateHr(doc, invoiceTableTop + 20);
    doc.font("Helvetica");
  
    for (i = 0; i < invoice.items.length; i++) {
      const item = invoice.items[i];
      const position = invoiceTableTop + (i + 1) * 30;
      this.generateTableRow(
        doc,
        position,
        item.item,
        item.description,
        this.formatCurrency(item.amount / item.quantity),
        item.quantity,
        this.formatCurrency(item.amount)
      );
  
      this.generateHr(doc, position + 20);
    }
  
    const subtotalPosition = invoiceTableTop + (i + 1) * 30;
    this.generateTableRow(
      doc,
      subtotalPosition,
      "",
      "",
      "Subtotal",
      "",
      this.formatCurrency(invoice.subtotal)
    );
  
    const paidToDatePosition = subtotalPosition + 20;
    this.generateTableRow(
      doc,
      paidToDatePosition,
      "",
      "",
      "Paid To Date",
      "",
      this.formatCurrency(invoice.paid)
    );
  
    const duePosition = paidToDatePosition + 25;
    doc.font("Helvetica-Bold");
    this.generateTableRow(
      doc,
      duePosition,
      "",
      "",
      "Balance Due",
      "",
      this.formatCurrency(invoice.subtotal - invoice.paid)
    );
    doc.font("Helvetica");
  }
  
  generateFooter(doc) {
    doc
      .fontSize(10)
      .text(
        "PT. TEMPA BERSAMA. Thank you for your business.",
        50,
        780,
        { align: "center", width: 500 }
      );
  }
  
  generateTableRow(
    doc,
    y,
    item,
    description,
    unitCost,
    quantity,
    lineTotal
  ) {
    doc
      .fontSize(10)
      .text(item, 50, y)
      .text(description, 150, y)
      .text(unitCost, 280, y, { width: 90, align: "right" })
      .text(quantity, 370, y, { width: 90, align: "right" })
      .text(lineTotal, 0, y, { align: "right" });
  }
  
  generateHr(doc, y) {
    doc
      .strokeColor("#aaaaaa")
      .lineWidth(1)
      .moveTo(50, y)
      .lineTo(550, y)
      .stroke();
  }
  
  formatCurrency(cents) {
    return "$" + (cents / 100).toFixed(2);
  }
  
  formatDate(date) {
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();
  
    return year + "/" + month + "/" + day;
  }

  async printProject(data, uidProject){
    return new Promise(async (resolve, reject) => {
      var docDefinition = {
        // pageOrientation: 'landscape',
        pageMargins: [ 10, 10 ],
        content: [],
        styles: {
          header: {
            fontSize: 18,
            bold: true,
            margin: [0, 0, 0, 10]
          },
          subheader: {
            fontSize: 16,
            bold: true,
            margin: [0, 10, 0, 5]
          },
          tableExample: {
            margin: [0, 5, 0, 15],
            bold: true
          },
          tableHeader: {
            bold: true,
            fontSize: 13,
            color: 'black'
          },
          fontStyle: {
            fontSize: 10
          }
        },
        defaultStyle: {
          fontSize: 7,
          bold: true
          // alignment: 'justify'
        }
      }

      for (let res of data) {
        if(res){
          res.listUser.unshift(
            [{
              text: 'NAMA SKILL',
              fontSize: 10
            }, {
              text: 'NAMA TUKANG',
              fontSize: 10
            }]
          )

          let val = {
            unbreakable: true,
            stack: [{
              style: "tableExample",
              table: {
                widths: ["*"],
                body: [
                  [
                    [{
                        text: `${res.name}`,
                        alignment: "center",
                        style: "fontStyle"
                      },
                      res.desc.NAMA ?
                      {
                        text: `NAMA : ${res.desc.NAMA}`,
                        style: "fontStyle"
                      } : null,
                      {
                        text: `NO POT : ${res.desc['NO POT']}`,
                        style: "fontStyle"
                      },
                      {
                        text: `NO SERI : ${res.desc['NO SERI']}`,
                        style: "fontStyle"
                      },
                      {
                        text: `ISI : ${res.desc.ISI}`,
                        style: "fontStyle"
                      },
                      {
                        style: 'tableExample',
                        margin: [0, 5, 0, 5],
                        table: {
                          widths: [150, '*'],
                          body: res.listUser
                        }
                      }
                    ],
                  ],
                ],
              },
            }]
          }
          docDefinition.content.push(val)
        }
      }

      var doc = printer.createPdfKitDocument(docDefinition);
      const tmp = Helpers.tmpPath()
      doc.pipe(fs.createWriteStream(tmp + '/' + uidProject + '_project' + '.pdf'));
      doc.end();
      return resolve('Success')
    })
  }

  buildTableBody(data, columns) {
    var body = [];

    body.push(columns);

    data.forEach((row) => {
      var dataRow = [];

      columns.forEach((column) => {
        dataRow.push(row[column].toString());
      })

      body.push(dataRow);
    });

    return body;
  }

  table(data, columns) {
    return {
      table: {
        widths: '*',
        headerRows: 1,
        style: '',
        body: this.buildTableBody(data, columns)
      }
    };
  }
}

module.exports = PdfService
