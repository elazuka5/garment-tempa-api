'use strict'

var jwt = use('jsonwebtoken');

const Redis = use('Redis')

class RedisService {
  async get(keyRed, index){
    try {
      if(index){
        const getBasedIndex = await Redis.hget(keyRed, index)
        if(getBasedIndex){
          const jsonIndex = JSON.parse(getBasedIndex)
          return jsonIndex
        }else{
          return 'INDEX NOT FOUND'
        }
      }else{
        const getBasedKey = await Redis.get(keyRed)
        if(getBasedKey){
          const jsonKey = JSON.parse(getBasedKey)
          return jsonKey
        }else{
          return 'KEY NOT FOUND'
        }
      }
    } catch (error) {
      return error
    }
  }

  // async update(keyRed, index, data){
  //   try {
  //     const checkRed = await Redis.hget(keyRed, index)
  //     if(checkRed){
  //       await Redis.hset(keyRed, index, JSON.stringify(data))
  //       return 'SUCCESS'
  //     }else{
  //       // await Redis.hset(keyRed, keyData, JSON.stringify(data))
  //       return 'NOT FOUND'
  //     }
  //   } catch (error) {
  //     return error
  //   }
  // }

  async update(keyRed, index, data){
    try {
      // const getData = await Redis.hget(keyRed, index)
      // if(getData){
        let qq = await Redis.lset(keyRed, index, JSON.stringify(data))
        console.log(234, qq);
        return 'SUCCESS'
      // }else{
      //   // await Redis.hset(keyRed, keyData, JSON.stringify(data))
      //   return 'NOT FOUND'
      // }
    } catch (error) {
      console.log(22222, error);
      return error
    }
  }
}

module.exports = RedisService
