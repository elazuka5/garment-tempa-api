'use strict'

const User = use('App/Models/User')
// const NotificationType = use('App/Models/NotificationType')
const apiKey = 'key=AAAA7LLoP1E:APA91bEZ9afZ9ztHpXqm3lxcgbOOwRHMej5O-M18u0r66e9VBB-ULZ9uWJwM2axLPohysWiecYqHql_YJ1lW8OzmqjBXqbV1ss7zRGTfZ2FfkcZl-X7cfI3V9PR9OTcHEBtSKYn6R8dK'
const axios = use('axios');

class ResponseService {
  async store(message, status, shortCode, body) {
    const res = {
      'body': body,
      'status': {
        'code': status,
        'message': message,
        'shortCode': shortCode
        }
      }
      return res
    }

    hasNull(target) {
      for (var member in target) {
        if (target[member] == null)
          return 1;
      }
      return 0;
    }

    chunkArr(array, size) {
      const chunked_arr = [];
      let copied = [...array]; // ES6 destructuring
      const numOfChild = Math.ceil(copied.length / size); // Round up to the nearest integer
      for (let i = 0; i < numOfChild; i++) {
        chunked_arr.push(copied.splice(0, size));
      }
      return chunked_arr;
    }

    paginator(items, current_page, per_page_items) {
      let page = current_page || 1,
      per_page = per_page_items || 10,
      offset = (page - 1) * per_page,
    
      paginatedItems = items.slice(offset).slice(0, per_page_items),
      total_pages = Math.ceil(items.length / per_page);
    
      return {
        total: items.length,
        perPage: per_page,
        page: page,
        pre_page: page - 1 ? page - 1 : null,
        next_page: (total_pages > page) ? page + 1 : null,
        total_pages: total_pages,
        data: paginatedItems
      };
    }

  // getAge(DOB) {
  //   var today = new Date();
  //   var birthDate = new Date(DOB);
  //   var age = today.getFullYear() - birthDate.getFullYear();
  //   var m = today.getMonth() - birthDate.getMonth();
  //   if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
  //       age = age - 1;
  //   }
  //   return age;
  // }

  // async strengthCheck(uid) {
  //   const user = await User.find(uid)
  //   await user.loadMany(['user_contacts', 'doctorContact', 'faceReq'])    
  //   const x = user.toJSON()
  //   console.log(x);

  //   var payload = []

  //   switch (user.userType) {
  //     case 4:
  //       if(!x.faceReq){
  //         payload.push('faceRecog')
  //       }
  //       if(!x.email || !x.fullName || !x.nik || !x.user_contacts.domicile || !x.user_contacts.gender || !x.user_contacts.address){
  //         payload.push('contacts')
  //       }
  //       break;
  //     case 3:
  //       if(!x.doctorContact.domicile || !x.doctorContact.gender || !x.doctorContact.address){
  //         payload.push('contacts')
  //       }
  //       break;
  //     default:
  //       break;
  //   }
  //   // if (user.userType === 4) {
  //   //   if (x.email === null && x.fullName === null && x.nik === null) {
  //   //     payload.push('credentials')
  //   //   }
  //   //   if (x.user_contacts.firstName === null) {
  //   //     payload.push('contact')
  //   //   }
  //     // if (x.talentExperiences.length === 0) {
  //     //   payload.push('experience')
  //     // }
  //     // if (x.talentExpertises.length === 0) {
  //     //   payload.push('expertise')
  //     // }
  //     // if (x.talentEducations.length === 0) {
  //     //   payload.push('education')
  //     // }
  //     // if (x.talentSkills.length === 0) {
  //     //   payload.push('skill')
  //     // }
  //   // }
  //   // } else {
  //   //   if (x.clientContact.address === null) {
  //   //     payload.push('contact')
  //   //   }
  //   // }
  //   return payload
  // }

  // async notif(payload) {
  //   // var target = null
  //   // if (payload.role === 0 && payload.to) {
  //   // if (payload.to) {
  //   // var target = await User.query()
  //   // .where('uid', payload.to)
  //   // .setVisible(['fcm'])
  //   // .first()

  //   // console.log('tar', target);
    
  //   var pushNotif = await NotificationType.query()
  //   // .with('notif', (builder) => {
  //   //   builder.with('from')
  //   //   // builder.with('to').with('from')
  //   // })
  //   .where('id', payload.type)
  //   .setVisible(['message', 'title'])
  //   .first()

  //   console.log('msg', pushNotif.toJSON());
  //   var mapObj = {
  //     "#from#": payload.from,
  //     "#to#": payload.to
  //   };
  //   var reg = new RegExp(Object.keys(mapObj).join("|"),"gi");
  //   var str = pushNotif.message.replace(reg, function(matched){
  //     return mapObj[matched];
  //   });
  //   var notifPayload = {
  //     to: payload.fcm,
  //     notification: {
  //       title : pushNotif.title,
  //       body : str,
  //       content_available : true,
  //       priority : 'high',
  //       icon: 'https://dashboard.mentalku.com/favicon.png',
  //       click_action: 'https://dashboard.mentalku.com/notification'
  //     }
  //   }

  //   var notifPayloadApps = {
  //     to: payload.fcmApps,
  //     title : pushNotif.title,
  //     body : str,
  //     sound : 'default',
  //     // notification: {
  //     //   content_available : true,
  //     //   priority : 'high',
  //     //   icon: 'https://dashboard.mentalku.com/favicon.png',
  //     //   click_action: 'https://dashboard.mentalku.com/notification'
  //     // }
  //   }
  //   try {            
  //     if(payload.fcm){
  //       await axios.post('https://fcm.googleapis.com/fcm/send', notifPayload, {
  //         headers: {
  //           'Authorization': apiKey,
  //           'Content-Type': 'application/json'
  //         }
  //       })
  //       console.log('FCM ADMIN')
  //     }
  //     if(payload.fcmApps){
  //       await axios.post('https://exp.host/--/api/v2/push/send', notifPayloadApps, {
  //         headers: {
  //           // 'Authorization': apiKey,
  //           'Content-Type': 'application/json'
  //         }
  //       })
  //       console.log('FCM USER')
  //     }
  //     console.log('notif success')
  //   } catch (error) {
  //     console.log(error)
  //   }
  // }

  // async notifGroup(role, operation, pushId) {    
  //   var notifPayload = {
  //     operation: operation,
  //     notification_key_name: roleGroup[role - 1],
  //     registration_ids: [pushId],
  //     notification_key: roleNotif[role - 1]
  //   }
  //   try {
  //     await axios.post('https://fcm.googleapis.com/fcm/notification', notifPayload, {
  //       headers: {
  //         'Authorization': apiKey,
  //         'Content-Type': 'application/json',
  //         'project_id': projectId
  //       }
  //     })
  //     console.log('add/remove success')

  //   } catch (error) {
  //     console.log(error)
  //   }
  // }

  // async addReward(type, uid, refId) {
  //   switch (type) {
  //     case 'applyJob':
  //       await Reward.create({
  //         userId: uid,
  //         desc: 'Apply job',
  //         point: 50
  //       })
  //       if (refId) {
  //         const refCheck = await Referral.find(refId)
  //         await Reward.create({
  //           userId: refCheck.sender,
  //           desc: 'Network applying job',
  //           point: 25
  //         })
  //       }
  //       break;
    
  //     default:
  //       break;
  //   }
  // }

  // async addVisitor(type, userId, ip, jobId) {
  //   switch (type) {
  //     case 'job':
  //       if (userId) {
  //         const checkUser = await Visitor.query().where('userId', userId).where('jobId', jobId).first()
  //         console.log(checkUser)
  //         if (!checkUser) {
  //           await Visitor.create({
  //             userId: userId,
  //             ip: ip || null,
  //             jobId: jobId
  //           })
  //         }
  //       } else {
  //         const checkIp = await Visitor.query().where('ip', ip).where('jobId', jobId).first()
  //         if (!checkIp) {
  //           await Visitor.create({
  //             userId: null,
  //             ip: ip || null,
  //             jobId: jobId
  //           })
  //         }
  //       }
  //       break;
    
  //     default:
  //       break;
  //   }
  // }
}

module.exports = ResponseService