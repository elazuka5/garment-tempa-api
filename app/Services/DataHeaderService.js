'use strict'

var jwt = use('jsonwebtoken');

// var moment = use('moment')
// const CryptoJS = use('crypto-js')
const { Base64 } = use('js-base64')
// const shortUrl = use('node-url-shortener')
// const signature = 'MENTALKU::2020'

class DataHeaderService {
  getHeaderData(data) {
    // return new Promise((resolve, reject) => {
      const newDataHeader = data.header('data')
    //   if(newDataHeader){
    //     let baseData = Base64.decode(newDataHeader)
    //     let pageData = JSON.parse(baseData)
    //     return resolve(pageData)
    //   }else{
    //     return null
    //   }
    // })
    // if(newDataHeader){
    //   let baseData = Base64.decode(newDataHeader)
    //   let pageData = JSON.parse(baseData)
    //   return pageData
    // }
    try {
      if(newDataHeader){
        let baseData = Base64.decode(newDataHeader)
        let pageData = JSON.parse(baseData)
        return pageData
      }
      // return new Promise((resolve, reject) => {
      //   if(newDataHeader){
      //     let baseData = Base64.decode(newDataHeader)
      //     let pageData = JSON.parse(baseData)
      //     return resolve(pageData)
      //   }else{
      //     return reject()
      //   }
      // })
    } catch (error) {
      return error
    }
  }

  async getHeaderAuth(data){
    try {
      const newDataHeader = data.header('Authorization')
      if(newDataHeader){
        const split = await newDataHeader.split(' ')
        const jwtToken = await jwt.decode(split[1])
        return jwtToken
      }
    } catch (error) {
      return error
    }
  }
}

module.exports = DataHeaderService
