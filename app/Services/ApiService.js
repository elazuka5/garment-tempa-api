'use strict'

var jwt = use('jsonwebtoken');
const axios = use('axios');

// var moment = use('moment')
// const CryptoJS = use('crypto-js')
const { Base64 } = use('js-base64')
// const shortUrl = use('node-url-shortener')
// const signature = 'MENTALKU::2020'

class ApiService {

  CHECK_FACE(payload, progress) {
    return new Promise((resolve, reject) => {
      const URL = 'http://128.199.212.147:5050/recognize'
      return axios.post(`${URL}`, payload, {
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/json",
        },
        onUploadProgress: (progressEvent) => {
          var percentCompleted = Math.floor((progressEvent.loaded * 100) / progressEvent.total);
          console.log(97, percentCompleted);
          // return progress && progress(percentCompleted)
        },
      }).then((res) => {
        console.log('res', res);
        if(res.data.status.code === 200){
          return resolve(res.data)
        }else{
          const err = res.data.status;
          return reject(err)
        }
      }).catch((err) => {
        console.log('errrrr', err);
        const error = err.response ? err.response.data.status : JSON.parse(JSON.stringify(err));
        return reject(error)
      })
    })
  }

  FACE(payload, progress) {
    return new Promise((resolve, reject) => {
      const URL = 'http://128.199.212.147:5050/register_face'
      return axios.post(`${URL}`, payload, {
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/json",
        },
        onUploadProgress: (progressEvent) => {
          // var percentCompleted = Math.floor((progressEvent.loaded * 100) / progressEvent.total);
          // return progress && progress(percentCompleted)
        },
      }).then((res) => {
        if(res.data.status.code === 200){
          return resolve(res.data)
        }else{
          const err = res.data.status;
          return reject(err)
        }
      }).catch((err) => {
        const error = err.response ? err.response.data.status : JSON.parse(JSON.stringify(err));
        return reject(error)
      })
    })
  }

  POST(URL, payload, progress) {
    return new Promise((resolve, reject) => {
      return axios.post(`${URL}`, payload, {
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/json",
        },
        onUploadProgress: (progressEvent) => {
          // var percentCompleted = Math.floor((progressEvent.loaded * 100) / progressEvent.total);
          // return progress && progress(percentCompleted)
        },
      }).then((res) => {
        if(res.status === 201){
          return resolve(res.data)
        }else{
          const err = res.data.status;
          return reject(err)
        }
      }).catch((err) => {
        const error = err.response ? err.response.data.status : JSON.parse(JSON.stringify(err));
        return reject(error)
      })
    })
  }
}

module.exports = ApiService
