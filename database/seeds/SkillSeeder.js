'use strict'

/*
|--------------------------------------------------------------------------
| SkillSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const skill = [
  { skillName: 'Potong Baju', price: 1000 },  
  { skillName: 'Potong Celana', price: 2000 },  
  { skillName: 'Jahit Baju', price: 3000 },  
  { skillName: 'Jahit Celana', price: 4000 },  
  { skillName: 'Obras Baju', price: 5000 },  
  { skillName: 'Obras Celana', price: 6000 },  
  { skillName: 'Rajut Baju', price: 7000 },  
  { skillName: 'Rajur Celana', price: 8000 },
  { skillName: 'Kancing Baju', price: 9000 },
  { skillName: 'Kancing Celana', price: 1000 },
  { skillName: 'Som', price: 2000 },
  { skillName: 'Buang Benang Baju', price: 3000 },
  { skillName: 'Buang Benang Celana', price: 4000 },
  { skillName: 'Gosok Baju', price: 5000 },
  { skillName: 'Gosok Celana', price: 6000 },
  { skillName: 'Packing', price: 7000 }
]

class SkillSeeder {
  async run () {
    await Factory.model('App/Models/MasterSkill').createMany(16, skill)
  }
}

module.exports = SkillSeeder
