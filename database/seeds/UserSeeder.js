'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

var type = [
  { id: 1, name: 'SuperAdmin', code: 1 },
  { id: 2, name: 'Admin', code: 2 },
  { id: 3, name: 'Tukang', code: 3 },
  { id: 4, name: 'Stakeholder', code: 4 },
  { id: 5, name: 'QC', code: 5 },
  { id: 6, name: 'Fitting', code: 6 }
];

var sprAdm = [
  { userName: 'superadmin', password: 'pass123', userType: 1 },
  { userName: 'superadmin1', password: 'pass123', userType: 1 },
  { userName: 'superadmin2', password: 'pass123', userType: 1 },
  { userName: 'superadmin3', password: 'pass123', userType: 1 },
  { userName: 'superadmin4', password: 'pass123', userType: 1 },
  { userName: 'superadmin5', password: 'pass123', userType: 1 }
];

class UserSeeder {
  async run () {
    
    await Factory.model('App/Models/UserType').createMany(type.length, type)
    await Factory.model('App/Models/User').createMany(sprAdm.length, sprAdm)
  }
}

module.exports = UserSeeder
