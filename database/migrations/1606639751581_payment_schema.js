'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PaymentSchema extends Schema {
  up () {
    this.create('payments', (table) => {
      table.string('uid').primary()
      // table.integer('invoiceId').unsigned().references('id').inTable('invoices')
      // table.string('invoiceNumber').nullable()
      table.integer('invoiceNumber').notNullable().defaultTo(0)
      table.string('userId').references('uid').inTable('users').onDelete('CASCADE')
      table.string('planProjectId').references('uid').inTable('planning_projects').onDelete('CASCADE')
      table.string('projectId').references('uid').inTable('projects').onDelete('CASCADE')
      table.string('skill').nullable()
      table.string('invoicePath').nullable()
      table.string('pathExcel').nullable()
      // table.boolean('isPaid').notNullable().defaultTo(false)
      table.timestamps()
    })
  }

  down () {
    this.drop('payments')
  }
}

module.exports = PaymentSchema
