'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MasterSkillSchema extends Schema {
  up () {
    this.create('master_skills', (table) => {
      table.increments()
      table.string('skillName').unique()
      table.integer('price').notNullable().defaultTo(0)
      table.timestamps()
    })
  }

  down () {
    this.drop('master_skills')
  }
}

module.exports = MasterSkillSchema
