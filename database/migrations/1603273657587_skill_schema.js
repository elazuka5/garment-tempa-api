'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SkillSchema extends Schema {
  up () {
    this.create('skills', (table) => {
      table.string('uid').primary()
      table.string('userId').references('uid').inTable('users').onDelete('CASCADE')
      table.text('skill').nullable()
      table.timestamp('created_at').defaultTo(this.fn.now())
      table.timestamp('updated_at').defaultTo(this.fn.now())
    })
  }

  down () {
    this.drop('skills')
  }
}

module.exports = SkillSchema
