'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      // table.integer('no').index()
      // table.index('no')
      // table.dropPrimary('no')
      table.string('uid').primary()
      table.integer('userType').unsigned().references('id').inTable('user_types')
      table.string('userName', 80).nullable().unique()
      table.string('password', 60).nullable()
      table.string('nip', 50).nullable().unique()
      table.string('phoneNumber', 100).nullable().unique()
      table.boolean('isDeleted').notNullable().defaultTo(false)
      table.boolean('isStaff').notNullable().defaultTo(false)
      table.timestamp('created_at').defaultTo(this.fn.now())
      table.timestamp('updated_at').defaultTo(this.fn.now())
      // table.primary('uid')
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
