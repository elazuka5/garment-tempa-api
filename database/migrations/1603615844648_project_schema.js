'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectSchema extends Schema {
  up () {
    this.create('projects', (table) => {
      table.string('uid').primary()
      // table.string('userId').references('uid').inTable('users').onDelete('CASCADE')
      table.string('projectId').references('uid').inTable('planning_projects').onDelete('CASCADE')
      // table.string('noPot').nullable()
      // table.string('noSeri').nullable()
      // table.string('name').nullable()
      // table.string('rank').nullable()
      // table.string('nip').nullable()
      // table.string('unity').nullable()
      // table.string('notes').nullable()
      table.text('description').nullable()
      table.text('size').nullable()
      table.text('skill').nullable()
      table.boolean('isScan').notNullable().defaultTo(false)
      table.integer('currentScan').notNullable().defaultTo(0)
      table.integer('indexSort').nullable()
      table.timestamp('created_at').defaultTo(this.fn.now())
      table.timestamp('updated_at').defaultTo(this.fn.now())
    })
  }

  down () {
    this.drop('projects')
  }
}

module.exports = ProjectSchema
