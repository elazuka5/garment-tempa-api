'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserContactSchema extends Schema {
  up () {
    this.create('user_contacts', (table) => {
      table.string('uid').primary()
      table.string('userId').references('uid').inTable('users').onDelete('CASCADE')
      // table.string('nik', 50).nullable().unique()
      table.string('fullName', 100).nullable()
      table.string('address', 100).nullable()
      table.string('gender', 100).nullable()
      // table.string('phoneNumber', 100).nullable().unique()
      table.timestamp('created_at').defaultTo(this.fn.now())
      table.timestamp('updated_at').defaultTo(this.fn.now())
    })
  }

  down () {
    this.drop('user_contacts')
  }
}

module.exports = UserContactSchema
