'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PlanningProjectSchema extends Schema {
  up () {
    this.create('planning_projects', (table) => {
      table.string('uid').primary()
      table.string('name').nullable()
      table.integer('day').nullable()
      table.integer('total').nullable()
      table.datetime('deadline').nullable()
      table.string('notes').nullable()
      table.string('status').nullable()
      table.string('companyName').nullable()
      table.text('size').nullable()
      table.text('skill').nullable()
      table.integer('totalProject').notNullable().defaultTo(0)
      table.integer('count').notNullable().defaultTo(0)
      table.boolean('isHold').notNullable().defaultTo(false)
      table.boolean('isDeleted').notNullable().defaultTo(false)
      table.timestamp('created_at').defaultTo(this.fn.now())
      table.timestamp('updated_at').defaultTo(this.fn.now())
    })
  }

  down () {
    this.drop('planning_projects')
  }
}

module.exports = PlanningProjectSchema
