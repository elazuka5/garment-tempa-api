'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectProcessSchema extends Schema {
  up () {
    this.create('project_processes', (table) => {
      table.string('uid').primary()
      table.string('userId').references('uid').inTable('users').onDelete('CASCADE')
      table.string('qcId').references('uid').inTable('users').onDelete('CASCADE')
      table.string('projectId').references('uid').inTable('projects').onDelete('CASCADE')
      // table.text('qc').nullable()
      // table.text('user').nullable()
      table.string('skill').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('project_processes')
  }
}

module.exports = ProjectProcessSchema
