'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectCommentSchema extends Schema {
  up () {
    this.create('project_comments', (table) => {
      table.increments()
      table.string('userId').references('uid').inTable('users').onDelete('CASCADE')
      table.string('planProjectId').references('uid').inTable('planning_projects').onDelete('CASCADE')
      table.string('projectId').references('uid').inTable('projects').onDelete('CASCADE')
      table.text('notes').nullable()
      table.timestamp('created_at').defaultTo(this.fn.now())
      table.timestamp('updated_at').defaultTo(this.fn.now())
    })
  }

  down () {
    this.drop('project_comments')
  }
}

module.exports = ProjectCommentSchema
