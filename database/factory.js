'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

Factory.blueprint('App/Models/User', async (faker, i, data) => {
  return {
    userName: data[i].userName,
    password: data[i].password,
    userType: data[i].userType
  }
});

Factory.blueprint('App/Models/UserType', async (faker, i, data) => {
  return {
    id: data[i].id,
    name: data[i].name,
    code: data[i].code
  }
});

Factory.blueprint('App/Models/MasterSkill', async (faker, i, data) => {
  return {
    skillName: data[i].skillName,
    price: data[i].price
  }
});
