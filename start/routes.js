'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'HI!' }
})

Route.group(() => {
  // ============ AUTH ================
  Route.post('login', 'AuthController.login')
  Route.get('logout', 'AuthController.logout').middleware(['auth'])
  Route.post('change-password', 'AuthController.changePass').middleware(['auth', 'checkToken'])
  Route.post('refresh/token', 'AuthController.refreshToken')
  // ============ AUTH ================

  // ============ USER ================
  Route.post('users/create', 'UserController.store').middleware(['auth', 'checkToken'])
  Route.get('users', 'UserController.index').middleware(['auth', 'checkToken'])
  Route.post('users/edit', 'UserController.update').middleware(['auth', 'checkToken']).validator(['StoreUser'])
  Route.get('users/detail', 'UserController.show').middleware(['auth', 'checkToken'])
  Route.post('users/delete', 'UserController.destroy').middleware(['auth', 'checkToken'])
  // ============ USER ================

  // ============ PLANNING PROJECT ================
  Route.get('plan/project', 'PlanningProjectController.indexPlanProject').middleware(['auth', 'checkToken'])
  Route.post('plan/project/create', 'PlanningProjectController.storePlanProject').middleware(['auth', 'checkToken'])
  Route.post('plan/project/edit', 'PlanningProjectController.updatePlanProject').middleware(['auth', 'checkToken'])
  Route.post('plan/project/delete', 'PlanningProjectController.destroyPlanProject').middleware(['auth', 'checkToken'])
  Route.get('plan/project/detail', 'PlanningProjectController.show').middleware(['auth', 'checkToken'])
  // Route.get('plan/project/user', 'PlanningProjectController.indexUser').middleware(['auth', 'checkToken'])
  // ============ PLANNING PROJECT ================
  
  // ============ PROJECT ================
  Route.get('project', 'ProjectController.index').middleware(['auth', 'checkToken'])
  Route.post('project/create', 'ProjectController.storeProject').middleware(['auth', 'checkToken'])
  Route.post('project/change-status', 'ProjectController.changeStatusProject').middleware(['auth', 'checkToken'])
  Route.post('project/upload', 'ProjectController.uploadProject').middleware(['auth', 'checkToken'])
  Route.post('project/edit', 'ProjectController.updateProject').middleware(['auth', 'checkToken'])
  Route.post('project/delete', 'ProjectController.deleteProject').middleware(['auth', 'checkToken'])
  Route.post('project/qr', 'ProjectController.printQR').middleware(['auth', 'checkToken'])
  Route.get('project/detail', 'ProjectController.showProject').middleware(['auth', 'checkToken'])
  Route.post('project/print', 'ProjectController.printProject').middleware(['auth', 'checkToken'])

  Route.post('project/size', 'ProjectController.updateSize').middleware(['auth', 'checkToken'])
  
  Route.post('project/edit-cache', 'ProjectController.updateCachedProject').middleware(['auth', 'checkToken'])

  Route.post('project/qr-code', 'ProjectController.storeQrCode').middleware(['auth', 'checkToken'])
  Route.post('project/scan', 'ProjectController.scanProject').middleware(['auth', 'checkToken'])
  Route.post('project/notes/create', 'ProjectController.storeComment').middleware(['auth', 'checkToken'])
  Route.post('project/notes/delete', 'ProjectController.deleteComment').middleware(['auth', 'checkToken'])
  // ============ PROJECT ================

  // ============ MASTER ============ ====
  Route.get('master', 'DataMasterController.datamaster')
  // ============ MASTER ================

  // ============ SKILL ================
  Route.get('skill', 'SkillController.index').middleware(['auth', 'checkToken'])
  Route.post('skill/create', 'SkillController.storeSkill').middleware(['auth', 'checkToken'])
  Route.post('skill/edit', 'SkillController.updateSkill').middleware(['auth', 'checkToken'])
  Route.get('skill/detail', 'SkillController.showSkill').middleware(['auth', 'checkToken'])
  Route.post('skill/delete', 'SkillController.deleteSkill').middleware(['auth', 'checkToken'])
  // ============ SKILL ================

  // ============ PAYMENT ================
  Route.get('payment', 'PaymentController.index').middleware(['auth', 'checkToken'])
  Route.get('payment/detail', 'PaymentController.show').middleware(['auth', 'checkToken'])
  
  Route.post('payment/create', 'PaymentController.payment').middleware(['auth', 'checkToken'])
  Route.post('invoice', 'PaymentController.pdfInvoice').middleware(['auth', 'checkToken'])
  Route.post('export', 'PaymentController.exportPayment').middleware(['auth', 'checkToken'])
  Route.post('export/excel', 'PaymentController.exportExcel').middleware(['auth', 'checkToken'])

  Route.get('payment/export', 'PaymentController.listPayment').middleware(['auth', 'checkToken'])
  // Route.post('plan/project/create', 'PlanningProjectController.storePlanProject').middleware(['auth', 'checkToken'])
  // Route.post('plan/project/edit', 'PlanningProjectController.updatePlanProject').middleware(['auth', 'checkToken'])
  // Route.post('plan/project/delete', 'PlanningProjectController.destroyPlanProject').middleware(['auth', 'checkToken'])
  // Route.get('plan/project/detail', 'PlanningProjectController.show').middleware(['auth', 'checkToken'])
  // Route.get('plan/project/user', 'PlanningProjectController.indexUser').middleware(['auth', 'checkToken'])
  // ============ PAYMENT ================

  // ============ INVOICE ================
  Route.get('invoice', 'InvoiceController.index').middleware(['auth', 'checkToken'])
  Route.get('invoice/detail', 'InvoiceController.show').middleware(['auth', 'checkToken'])
  // Route.get('payment/detail', 'PaymentController.show').middleware(['auth', 'checkToken'])
  
  // Route.post('payment/create', 'PaymentController.payment').middleware(['auth', 'checkToken'])
  // Route.post('invoice', 'PaymentController.pdfInvoice').middleware(['auth', 'checkToken'])
  // Route.post('export', 'PaymentController.exportPayment').middleware(['auth', 'checkToken'])

  // Route.get('payment/export', 'PaymentController.listPayment').middleware(['auth', 'checkToken'])
  // Route.post('plan/project/create', 'PlanningProjectController.storePlanProject').middleware(['auth', 'checkToken'])
  // Route.post('plan/project/edit', 'PlanningProjectController.updatePlanProject').middleware(['auth', 'checkToken'])
  // Route.post('plan/project/delete', 'PlanningProjectController.destroyPlanProject').middleware(['auth', 'checkToken'])
  // Route.get('plan/project/detail', 'PlanningProjectController.show').middleware(['auth', 'checkToken'])
  // Route.get('plan/project/user', 'PlanningProjectController.indexUser').middleware(['auth', 'checkToken'])
  // ============ INVOICE ================

  Route.post('document', 'ProjectController.document').middleware(['auth', 'checkToken'])
  Route.get('dashboard', 'ProjectController.dashboard').middleware(['auth', 'checkToken'])


}).prefix('api/v1')

